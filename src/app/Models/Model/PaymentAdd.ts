import { TypePaiement } from 'app/Enums/TypePaiement.Enum';
import { Typepaiement } from 'app/components/paiement-facture/paiement.component';

export class PaymentAdd {
	description: string;
	operation: TypePaiement;
	type: Typepaiement;
	datePayment: Date;
	amount: number;
	accountId: number;
	paymentMethodId: number;
	creditNoteId: string;
	invoices: Invoices[];
	expenses: Expenses[];
}


export class Expenses {
	documentId: string;
	amount: number;
}

export class Invoices {
	documentId: string;
	amount: number;
}
