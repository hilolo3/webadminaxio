import { TypeValue } from 'app/Enums/typeValue.Enums';
import { ProductSupplier } from './ProductSuppplier';

export class MinimalProductDetails {
	id: string;
	reference: string;
	designation: string;
	description: string;
	name: string;
	totalHours: string;
	materialCost: string;
	hourlyCost: string;
	vat: string;
	totalHT: string;
	totalTTC: string;
	unite: string;
	categorieId: string;
	discount: Discount;
	productSuppliers: ProductSupplier[]

}

export class Discount {
	type: TypeValue;
	value: string;
}


