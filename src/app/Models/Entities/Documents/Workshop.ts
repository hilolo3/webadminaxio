import { Client } from '../Contacts/Client';
import { Historique } from '../Commun/Historique';
import { Rubrique } from './Rubrique';


export class Workshop {
	id: string;
	name: string;
	description: string;
	comment: string;
	createdOn: Date;
	status: string;
	totalHours: number;
	amount: number;
	progressRate: number;
	changesHistory: Historique[];
	client: Client;
	Rubrics: Rubrique[];
}
