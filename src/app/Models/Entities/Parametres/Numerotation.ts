import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';

export class Numerotation {
	prefix: string;
	dateFormat: number;
	counter: number;
	counterLength: number;
	documentType: TypeNumerotation;
}

export class Parametrage {
	id: number;
	contenu: string;
	type: number;
}

export class ParametrageContenuDevis {
	note: string;
	conditionReglement: string;
	objet: string;
	note_facture: string;
	conditions_facture: string;
	objet_facture: string;
	validite_facture: any;
	validite_boncommande: any;
}

export enum FormatDate {
	None = '0',
	Annee = '1',
	AnneeMois = '2',
}