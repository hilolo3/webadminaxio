import { Contact } from '../Commun/Contact';
import { Historique } from '../Commun/Historique';
import { Memo } from '../Commun/Memo';
import { Adresse } from '../Commun/Adresse';
import { ClientType } from 'app/Enums/TypeClient.enum';

export class Client {
	id: string;
	code: string;
	addresses: Adresse[];
	reference: string;
	firstName: string;
	lastName: string;
	name?: string;
	groupeId;
	phoneNumber: string;
	fax: string;
	landLine?: string;
	email: string;
	website: string;
	siret: string;
	intraCommunityVAT: string;
	accountingCode: string;
	contactInformations: Contact[];
	changesHistory: Historique[];
	additionalInformations: string;
	memos: Memo[];
	type: ClientType;
	prenom: string;
	note: string;
	paymentCondition: string;
	materials: [];

}

export class ClientMinimalInfo {
	id: string;
	reference: string;
	name: string;
	billingAddress: Adresse;
}
