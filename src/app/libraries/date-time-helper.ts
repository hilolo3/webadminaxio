import { DatePipe } from '@angular/common';

export class DateTimeHelper {


	/**
	 * sort list by date
	 */
	public static sortListByDate = (a: any, b: any) =>
		new Date(a.dateCreation) > new Date(b.dateCreation) ? 1 : -1

	/**
	 * validate date1 > date2
	 */
	public static validerDates(date1: string, date2: string) {
		date1 = this.setFormatDateStartYear(new Date(date1));
		date2 = this.setFormatDateStartYear(new Date(date2));
		return new Date(date1) >= new Date(date2);
	}

	public static getFormatDate() {
		return 'dd/MM/yyyy';
	}
	/**
	 * format date by company's format
	 */
	// public static formatDate(date: any) {
	// 	// check valid date
	// 	return new DatePipe(localStorage.getItem(LocalElements.currentLanguage)).transform(date, this.getFormatDate());
	// }

	// public static pdfFormatDate(date: string) {
	// 	return new DatePipe(localStorage.getItem(LocalElements.currentLanguage)).transform(date, 'dd-MM-yyyy');
	// }

	/**
	 * current date to string
	 */
	public static getDateNowForId() {
		const dateNow = new Date();
		// tslint:disable-next-line:max-line-length
		return `${dateNow.getFullYear()}${(dateNow.getMonth() + 1).toString().padStart(2, '0')}${(dateNow.getDay() + 1).toString().padStart(2, '0')}${(dateNow.getHours()).toString().padStart(2, '0')}${dateNow.getMinutes().toString().padStart(2, '0')}${dateNow.getSeconds().toString().padStart(2, '0')}${dateNow.getMilliseconds().toString().padStart(4, '0')}`;
	}


	public static getFormatDateInput(dateToFormat: Date) {
		return (
			dateToFormat.getFullYear() +
			'-' +
			String(dateToFormat.getMonth() + 1).padStart(2, '0') +
			'-' +
			String(dateToFormat.getDate()).padStart(2, '0')
		);
	}


	/**
	 * Formats a date input
	 *
	 * @param dateToFormat Starting date
	 * @param devider Separator
	 */
	public static setFormatDateStartYear(dateToFormat: Date, devider: string = '/') {

		return (
			String((dateToFormat || new Date()).getFullYear()).padStart(2, '0') +
			devider +
			String((dateToFormat || new Date()).getMonth() + 1).padStart(2, '0') +
			devider +
			String((dateToFormat || new Date()).getDate()).padStart(2, '0'));
	}

	static setFormatDateStartDay(dateToFormat: Date, devider: string = '/') {
		if (typeof dateToFormat === 'string') { dateToFormat = new Date(dateToFormat); }
		return (
			String(dateToFormat.getDate()).padStart(2, '0') +
			devider +
			String(dateToFormat.getMonth() + 1).padStart(2, '0') +
			devider +
			dateToFormat.getFullYear()
		);

	}


	static dateAddDays(nbrJrs: number, date: Date) {
		return new Date(date.getTime() + nbrJrs * 24 * 60 * 60 * 1000);
	}


	/**
	 * Check date is today
	 * @param dateToCheck
	 */
	public static isToday = (dateToCheck: string) =>
		new Date(dateToCheck).toISOString().split('T')[0] === new Date().toISOString().split('T')[0]

}

