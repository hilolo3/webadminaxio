import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParameteresRoutingModule } from './parameteres-routing.module';
import { NumerotationPrefixeModule } from './numerotation-prefixe/numerotation-prefixe.module';
import { PrixModule } from './prix/prix.component.module';
import { TvaModule } from './tva/tva.component.module';
import { MessagerieModule } from './messagerie/Messagerie.module';
import { HoraireTravailModule } from './horaire-travail/horaire-travail.module';
import { SyncAgendaGoogleModule } from './sync-agenda-google/sync-agenda-google.module';
import { ParametrageDocumentModule } from './parametrage-document/parametrage-document.module';
import { CalendarModule } from 'app/custom-module/primeng/primeng';
import { FormsModule } from '@angular/forms';
import { CommonModules } from 'app/common/common.module';
import { ParametrageComptabiliteModule } from './comptabilite/comptabilite.module';
import { ObjectifsCaModule } from './objectifs-ca/objectifs-ca.module';
import { GenerateContractModule } from './generate-contract/generate-contract.module';
import { ParametragePdfModule } from './parametrage-pdf/parametrage-pdf.module';
import { MatExpansionModule } from '@angular/material';
import { AgendaModule } from './agenda/agenda.module';
import { HttpClient } from '@angular/common/http';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

export function createTranslateLoader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/status/', suffix: '.json' },
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
		{ prefix: './assets/i18n/parametres/', suffix: '.json' }
	]);
}

@NgModule({
	providers: [],
	declarations: [],
	imports: [
		CommonModule,
		ParameteresRoutingModule,
		NumerotationPrefixeModule,
		PrixModule,
		TvaModule,
		ParametrageDocumentModule,
		MessagerieModule,
		HoraireTravailModule,
		SyncAgendaGoogleModule,
		ParametrageComptabiliteModule,
		FormsModule,
		CommonModules,
		AgendaModule,
		CalendarModule,
		ObjectifsCaModule,
		GenerateContractModule,
		ParametragePdfModule,
		MatExpansionModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient]
			},
			isolate: true
		}),

	]
})
export class ParameteresModule { }
