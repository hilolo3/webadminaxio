import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'app-name-model',
	templateUrl: './name-model.component.html'
})
export class NameModelComponent implements OnInit {

	form: FormGroup;
	@Input() name = '';

	constructor(
		private formBuilder: FormBuilder,
		public modal: NgbActiveModal
	) {
		this.form = this.formBuilder.group({})
	}

	ngOnInit() {
		this.form.addControl('name', new FormControl('', Validators.required));
	}

	save() {
		if (this.form.valid) {
			this.modal.close(this.form.value);
		}
	}

}
