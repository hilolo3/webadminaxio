import { Component, OnInit, Inject } from '@angular/core';
import { HelperFunctions } from 'app/libraries/helper';
import { TranslateService } from '@ngx-translate/core';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { DialogChangeType, DialogService } from 'app/utils/dialog/dialog.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SubCategory } from 'app/Models/Model/SubCategory';
import { SubClassification } from 'app/Models/Model/SubClassification';



declare var swal: any;
declare var toastr: any;


@Component({
	selector: 'app-classification-vente',
	templateUrl: './classification-vente.component.html',
	styleUrls: ['./classification-vente.component.scss']
})
export class ClassificationVenteComponent implements OnInit {



	subCategory: any;
	modalSubClassificationState = false;
	modalClassificationState = false;
	type: string;
	HeaderSubClassification: string;
	HeaderClassification: string;
	categoryDepense: any;
	subCategorySales: Array<SubCategory>;
	loading = false;
	classifications: any;
	classificationForm: FormGroup;
	subClassificationForm: FormGroup;
	salesForm: FormGroup;
	addTranslationState = false;
	subClassifications: Array<SubClassification>;
	subClassification: SubClassification;
	modalSalesState = false;
	headerSales: string;
	currentClassification: any;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private dialogService: DialogService,
		private formBuilder: FormBuilder,
	) { }


	/** 
   * Init component
   **/
	async ngOnInit() {
		await this.getSales();
		this.HeaderSubClassification = 'Ajouter une sous-classification';
		this.HeaderClassification = 'Ajouter une classification';
		this.headerSales = 'Ajouter une Vente'
		this.type = 'sales';
		await this.initSubClassificationForm();
		await this.initFormClassification();
	}


	/** 
	  * Get all Sales( ventes  ) classification
	  **/
	async getSales() {
		this.classifications = (await this.service.getAll(ApiUrl.configClassificationSales).toPromise()).value;
	}


	/** 
	* Init form classification
	**/
	async initFormClassification() {
		this.classificationForm = this.formBuilder.group({
			label: ['', [Validators.required]],
			id: [null],
		});
	}


	/**
  * On classification add
  **/
	async onClassificationAdd() {
		this.modalClassificationState = true;
	}


	/** 
	* On delete classification
	* @param e The event object of the statut filter
	* @param index The classification index
	**/
	onClassificationDelete(e: MouseEvent, index: number) {

		const currentClassification = this.classifications[index];
		this.translate.get('parametresCompatibility.delete').subscribe(text => {
			swal({
				title: text.title,
				text: text.questionClassification,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: false
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: false
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.service.delete(ApiUrl.configClassification, currentClassification.id)
						.subscribe(res => {
							if (res) {
								this.getSales();
								swal(text.success, '', 'success');
							} else {
								swal(text.error, '', 'error');
							}

						});
				} else {
					swal(text.cancel, '', 'error');
				}
			});
		});

	}


	/** 
	 * On update Classification
	 * @param e The event object of the statut filter
	 * @param index The classification 
	 **/
	async onClassificationUpdate(e: MouseEvent, index: number) {

		const updatedClasifcation = this.classifications[index];
		this.classificationForm.controls['label'].setValue(updatedClasifcation.label);
		this.classificationForm.controls['id'].setValue(updatedClasifcation.id);
		this.modalClassificationState = true;
	}

	/**
	 * Add and update classification 
	 **/
	changeClassification() {
		if (!this.classificationForm.valid) {
			return false;
		}

		this.classifications = [{
			id: this.classificationForm.value.id,
			type: this.type,
			label: this.classificationForm.value.label,
			description: null,
			parentId: null,
		}]

		this.service.updateAll(ApiUrl.configClassification, this.classifications).
			subscribe(res => {
				this.loading = false;
				this.translate.get('parametresCompatibility.form').subscribe(text => {

					toastr.success(text.addd.successClassification, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
				this.modalClassificationState = false;
				this.classificationForm.reset();
				this.getSales();
			}, err => {
				this.loading = false;
				this.translate.get('parametresCompatibility.form').subscribe(text => {
					toastr.error(text.addd.errorClassification, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
			})
	}


	/** 
	* Init form sub classification
	**/
	async initSubClassificationForm() {
		const categoriesDepense = (await this.service.getAll(ApiUrl.configCategory).toPromise()).value;
		this.subCategorySales = categoriesDepense.filter(x => x.type === 2)[0].subCategories;
		this.subClassificationForm = this.formBuilder.group({
			id: [null],
			label: ['', [Validators.required]],
			chartAccountItemId: ['', [Validators.required]],
			description: ['']
		});
	}

	/** 
	 * On sub classfification add
	 * @param e The event object of the statut filter
	 * @param index The sub classification index
	 **/
	onSubClassificationAdd(e: MouseEvent, index: number) {
		e.stopPropagation();
		this.subClassificationForm.reset();
		this.currentClassification = this.classifications[index];
		this.modalSubClassificationState = true;
	}

	/**
	 * On update sub classification
	 * @param index The sub classification index
	 * @param parent The classification index 
   **/
	onSubClassificationUpdate(index: number, parent: number) {

		this.currentClassification = this.classifications[parent];
		const subClassification = this.currentClassification['subClassification'][index];
		this.subClassificationForm.controls['label'].setValue(subClassification.label);
		this.subClassificationForm.controls['id'].setValue(subClassification.id);
		this.subClassificationForm.controls['chartAccountItemId'].setValue(subClassification.chartAccountItem.id);
		this.subClassificationForm.controls['description'].setValue(subClassification.description);
		this.modalSubClassificationState = true;

	}

	/** 
  * On delete sub classification
  * @param index The sub classification index
  * @param parent The classification index
  **/
	subClassificationDelete(index: number, parent: number) {

		const deletedSubClassification = this.classifications[parent]['subClassification'][index];

		/* what that the id you need  */
		this.translate.get('parametresCompatibility.delete').subscribe(text => {
			swal({
				title: text.title,
				text: text.questionSubClassification,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: false
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: false
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.service.delete(ApiUrl.configClassification, deletedSubClassification.id)
						.subscribe(res => {
							if (res) {
								this.getSales();
								swal(text.success, '', 'success');
							} else {
								swal(text.error, '', 'error');
							}

						});
				} else {
					swal(text.cancel, '', 'error');
				}
			});
		});

	}

	/** 
	* Add  update sub classification
	**/
	changeSubClassification() {
		if (!this.subClassificationForm.valid) {
			return false;
		}

		this.subClassifications = [{
			id: this.subClassificationForm.value.id,
			type: this.type,
			label: this.subClassificationForm.value.label,
			description: this.subClassificationForm.value.description,
			chartAccountItemId: this.subClassificationForm.value.chartAccountItemId,
			parentId: this.currentClassification.id,

		}]

		this.service.updateAll(ApiUrl.configClassification, this.subClassifications).
			subscribe(res => {
				this.loading = false;
				this.translate.get('parametresCompatibility.form').subscribe(text => {
					toastr.success(text.addd.success, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
				this.modalSubClassificationState = false;
				this.subClassificationForm.reset();
				this.getSales();
			}, err => {
				this.loading = false;
				this.translate.get('parametresCompatibility.form').subscribe(text => {
					toastr.success(text.addd.success, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
			})
	}


	/** 
	* On add sales
	**/
	async onAddSale() {
		await this.initFormSales();
		this.modalSubClassificationState = false;
		this.modalSalesState = true;
	}


	/** 
	* Init form  sales
	**/
	async initFormSales() {
		this.salesForm = this.formBuilder.group({
			label: ['', [Validators.required]],
			code: ['', [Validators.required]],
		});
	}

	/**
	 * Add sales from classification sales
	 * **/
	addSale() {
		const resCatgorie = {
			'id': '',
			'type': 2,
			'categoryType': 0,
			'label': this.salesForm.value.label,
			'code': this.salesForm.value.code,
			'description': '',
			'vatValue': 0,
			'parentId': 3,
			'subCategories': []
		}

		this.service.updateAll(ApiUrl.configCategory, [resCatgorie])
			.subscribe(res => {

				this.translate.get('parametrageplanComptable').subscribe(text => {
					toastr.success(text.msgUpdate, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
				this.initSubClassificationForm();
				this.salesForm.reset();
				this.modalSubClassificationState = true;
				this.modalSalesState = false;
			});
	}

}
