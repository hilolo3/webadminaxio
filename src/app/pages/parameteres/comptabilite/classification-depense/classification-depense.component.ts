import { Component, OnInit, Inject } from '@angular/core';
import { HelperFunctions } from 'app/libraries/helper';
import { TranslateService } from '@ngx-translate/core';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { DialogChangeType, DialogService } from 'app/utils/dialog/dialog.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SubCategory } from 'app/Models/Model/SubCategory';
import { SubClassification } from 'app/Models/Model/SubClassification';

declare var swal: any;
declare var toastr: any;


@Component({
	selector: 'app-classification-depense',
	templateUrl: './classification-depense.component.html',
	styleUrls: ['./classification-depense.component.scss']
})
export class ClassificationDepenseComponent implements OnInit {

	//responseData = { "value": { "_id": "chm07kj7v8a85::parametres::classificationDepense", "_rev": "2-71061db3445e57944bef3709595362b5", "categories": [{ "id": "1", "nom": "Communication et marketing" }, { "id": "10", "nom": "Frais Bancaires" }, { "id": "11", "nom": "Frais du Personnel" }, { "id": "12", "nom": "Impôts et Taxes" }, { "id": "13", "nom": "Autres Charges" }, { "id": "2", "nom": "Frais de commercialisation" }, { "id": "3", "nom": "Frais de bureau" }, { "id": "4", "nom": "Locaux et batiments" }, { "id": "5", "nom": "Vehicules et autres" }, { "id": "6", "nom": "Immobilisation" }, { "id": "7", "nom": "Prestation de services" }, { "id": "8", "nom": "Marchandises" }, { "id": "9", "nom": "Assurances et Honoraires" }], "sousCategories": [{ "categorie": "1", "codeComptable": "623", "description": "Flyers, publicité dans les magazines, pages jaunes, outils de promotion, etc.", "id": "1", "nom": "Dépenses Publicité" }, { "categorie": "3", "codeComptable": "626", "description": "Chaises, tables, lampes, plantes... ATTENTION, si la facture est inférieure à 500 €, il faut choisir \"Fournitures de bureau", "id": "10", "nom": "Matériel de bureau" }, { "categorie": "3", "codeComptable": "626", "description": "Livres et magazines", "id": "11", "nom": "Livres et magazines" }, { "categorie": "4", "codeComptable": "613", "description": "Loyer (chauffage, électricité non inclus)", "id": "12", "nom": "Loyer" }, { "categorie": "4", "codeComptable": "606", "description": "Frais liés au chauffage et à l'air conditionné ", "id": "13", "nom": "Chauffage et ventilation" }, { "categorie": "4", "codeComptable": "606", "description": "Affranchissement ", "id": "14", "nom": "Affranchissement" }, { "categorie": "4", "codeComptable": "606", "description": "Frais liés au gaz", "id": "15", "nom": "Gaz/fioul" }, { "categorie": "4", "codeComptable": "606", "description": "Frais liés à l'électricité", "id": "16", "nom": "Électricité" }, { "categorie": "4", "codeComptable": "606", "description": "Frais liés à l'eau", "id": "17", "nom": "Eau" }, { "categorie": "4", "codeComptable": "635", "description": "Frais liés au regroupement et au ramassage des déchets", "id": "18", "nom": "Déchets" }, { "categorie": "4", "codeComptable": "615", "description": "Frais liés au nettoyage des bureaux et des bâtiments", "id": "19", "nom": "Nettoyage" }, { "categorie": "1", "codeComptable": "623", "description": "Cadeaux clientèle", "id": "2", "nom": "Cadeaux clientèle" }, { "categorie": "4", "codeComptable": "615", "description": "Maintenance des bureaux, par exemple rénovation, entretien du chauffage, etc.", "id": "20", "nom": "Maintenance" }, { "categorie": "5", "codeComptable": "615", "description": "Frais de maintenance des véhicules, les révisions, contrôle techniques...", "id": "21", "nom": "Frais liés à l'entretien du véhicule" }, { "categorie": "5", "codeComptable": "613", "description": "Location voiture", "id": "23", "nom": "Location véhicule" }, { "categorie": "5", "codeComptable": "635", "description": "Taxes voiture, par exemple taxes d'enregistrement, taxes environnementales, etc.", "id": "24", "nom": "Taxes véhicule" }, { "categorie": "5", "codeComptable": "612", "description": "leasing/crédit bail véhicules", "id": "25", "nom": "leasing véhicules ou Crédit bail" }, { "categorie": "5", "codeComptable": "218", "description": "Achat de voiture", "id": "26", "nom": "Achat de voiture et autre..." }, { "categorie": "5", "codeComptable": "606", "description": "Essence, Gazoil, GPL... ", "id": "27", "nom": "Carburants" }, { "categorie": "6", "codeComptable": "211", "description": "Terrains", "id": "28", "nom": "Terrains" }, { "categorie": "6", "codeComptable": "213", "description": "Bâtiments sur terrain privé", "id": "29", "nom": "Bâtiments" }, { "categorie": "1", "codeComptable": "623", "description": "Foires, expositions, présentations, etc.", "id": "3", "nom": "Frais de représentation" }, { "categorie": "6", "codeComptable": "205", "description": "Brevets et autres droits ", "id": "30", "nom": "Brevets et droits" }, { "categorie": "6", "codeComptable": "215", "description": "Équipements techniques, licenses logiciels et autres utilisés pour l'exploitation", "id": "31", "nom": "Équipements, machines et logiciels" }, { "categorie": "6", "codeComptable": "275", "description": "Dépôts de garantie", "id": "32", "nom": "Cautions et depôts de garanties" }, { "categorie": "7", "codeComptable": "611", "description": "Sous-traitance pour les produits manufacturés", "id": "33", "nom": "Sous-traitance" }, { "categorie": "7", "codeComptable": "604", "description": "Toutes prestations effectuées par des sociétés externes.", "id": "34", "nom": "Prestations externes" }, { "categorie": "7", "codeComptable": "622", "description": "Experts comptables, comptables, avocats, conseils et autres prestataires.", "id": "35", "nom": "Conseils et honoraires" }, { "categorie": "8", "codeComptable": "607", "description": "Achat de marchandises déstinée à la revente ou utilisées comme matière première.", "id": "36", "nom": "Marchandises" }, { "categorie": "9", "codeComptable": "616", "description": "Primes d'assurance", "id": "37", "nom": "Primes d'assurance" }, { "categorie": "9", "codeComptable": "658", "description": "Cotisations à des associations ou à des syndicats", "id": "38", "nom": "Cotisations associatives et syndicales" }, { "categorie": "9", "codeComptable": "616", "description": "Assurances véhicules", "id": "39", "nom": "Assurances véhicules" }, { "categorie": "2", "codeComptable": "624", "description": "frais de port et les autres frais liés au transport", "id": "4", "nom": "Frais de port" }, { "categorie": "9", "codeComptable": "624", "description": "Assurances transport de marchandises", "id": "40", "nom": "Assurance marchandise" }, { "categorie": "10", "codeComptable": "627", "description": "Frais de banque", "id": "41", "nom": "Frais financiers (tenue de compte)" }, { "categorie": "10", "codeComptable": "661", "description": "Intérêts débiteurs sur compte  bancaire ", "id": "42", "nom": "Intérêts débiteurs (agios)" }, { "categorie": "11", "codeComptable": "641", "description": "Salaires des employés ", "id": "43", "nom": "Salaires" }, { "categorie": "11", "codeComptable": "645", "description": "Urssaf, prévoyance, retraite, mutuelle...  ", "id": "44", "nom": "Charges du personnel" }, { "categorie": "11", "codeComptable": "621", "description": "Salaires des employés interimaires ", "id": "45", "nom": "Personnel interimaire" }, { "categorie": "11", "codeComptable": "641", "description": "Toutes dépenses liées à l'exploitant", "id": "46", "nom": "Charges de l'exploitant " }, { "categorie": "11", "codeComptable": "625", "description": "Déplacements des employés, voyages en avion ou en train, véhicules, repas et hébergements.", "id": "47", "nom": "Frais professionnels (employés)" }, { "categorie": "11", "codeComptable": "625", "description": "Déplacements du chef d'entreprise, voyages en avion ou en train, véhicules, repas et hébergements. ", "id": "48", "nom": "Frais professionnels (gérant)" }, { "categorie": "11", "codeComptable": "622", "description": "Frais liés aux formations du personnel  ", "id": "49", "nom": "Frais de formation" }, { "categorie": "2", "codeComptable": "608", "description": "Emballages Comptes", "id": "5", "nom": "Emballages" }, { "categorie": "12", "codeComptable": "635", "description": "PV, Amendes diverses... ", "id": "50", "nom": "Amendes" }, { "categorie": "12", "codeComptable": "695", "description": "Impôt sur les sociétés", "id": "51", "nom": "Impôt sur les sociétés" }, { "categorie": "12", "codeComptable": "635", "description": "Cotisations foncières des entreprises (CFE)  ", "id": "52", "nom": "Cotisations fonciéres des entreprises (CFE)" }, { "categorie": "12", "codeComptable": "635", "description": "Taxes douanières", "id": "53", "nom": "Taxes douanières" }, { "categorie": "13", "codeComptable": "658", "description": "Autres charges de gestion que vous n'arrivez pas à classer ", "id": "54", "nom": "Autres charges de gestion" }, { "categorie": "3", "codeComptable": "626", "description": "Fournitures de bureau, par exemple le papier, les toner, les stylos…", "id": "6", "nom": "Fournitures de bureau" }, { "categorie": "3", "codeComptable": "626", "description": "Affranchissement ", "id": "7", "nom": "Affranchissement" }, { "categorie": "3", "codeComptable": "626", "description": "Téléphones mobiles et fixes", "id": "8", "nom": "Téléphone et communications" }, { "categorie": "3", "codeComptable": "626", "description": "Frais liés à Internet, y compris les frais liés au nom de domaine", "id": "9", "nom": "Internet" }], "docType": "parametres", "isDeleted": false, "createdOn": "2020-06-03T12:29:54.0488171+02:00", "lastModifiedOn": "2020-06-03T12:29:54.0488113+02:00", "searchTerms": null }, "status": 1, "message": "Operation Succeeded", "messageCode": 1, "error": null };


	subCategory: any;
	modalSubClassificationState: boolean = false;
	modalClassificationState: boolean = false;
	type: string;
	HeaderSubClassification: string;
	HeaderClassification: string;
	categoryDepense: any;
	subCategoryDepense: Array<SubCategory>;
	loading: boolean = false;
	classifications: any;
	classificationForm: FormGroup;
	subClassificationForm: FormGroup;
	expenseForm: FormGroup;
	addTranslationState: boolean = false;
	subClassifications: Array<SubClassification>;
	subClassification: SubClassification;
	modalExpenseState: boolean = false;
	headerExpense: string;
	currentClassification: any;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private dialogService: DialogService,
		private formBuilder: FormBuilder,
	) { }


	/** 
   * Init component
   **/
	async ngOnInit() {
		await this.getExpense();
		this.HeaderSubClassification = "Ajouter une sous-classification";
		this.HeaderClassification = "Ajouter une classification";
		this.headerExpense = "Ajouter une dépenses"
		this.type = 'expense';
		await this.initSubClassificationForm();
		await this.initFormClassification();
	}


	/** 
	  * Get all Expense( depens  ) classification
	  **/
	async getExpense() {
		this.classifications = (await this.service.getAll(ApiUrl.configClassificationExpenses).toPromise()).value;
	}


	/** 
	* Init form classification
	**/
	async initFormClassification() {
		this.classificationForm = this.formBuilder.group({
			label: ['', [Validators.required]],
			id: [null],
		});
	}


	/**
  * On classification add
  **/
	async onClassificationAdd() {
		this.modalClassificationState = true;
	}


	/** 
	* On delete classification
	* @param e The event object of the statut filter
	* @param index The classification index
	**/
	onClassificationDelete(e: MouseEvent, index: number) {

		const currentClassification = this.classifications[index];
		this.translate.get('parametresCompatibility.delete').subscribe(text => {
			swal({
				title: text.title,
				text: text.questionClassification,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: false
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: false
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.service.delete(ApiUrl.configClassification, currentClassification.id)
						.subscribe(res => {
							if (res) {
								this.getExpense();
								swal(text.success, '', 'success');
							} else {
								swal(text.error, '', 'error');
							}

						});
				} else {
					swal(text.cancel, '', 'error');
				}
			});
		});

	}


	/** 
	 * On update Classification
	 * @param e The event object of the statut filter
	 * @param index The classification
	 **/
	async onClassificationUpdate(e: MouseEvent, index: number) {

		const updatedClasifcation = this.classifications[index];
		this.classificationForm.controls['label'].setValue(updatedClasifcation.label);
		this.classificationForm.controls['id'].setValue(updatedClasifcation.id);
		this.modalClassificationState = true;
	}

	/**
	 * Add and update classification 
	 **/
	changeClassification() {
		if (!this.classificationForm.valid) {
			return false;
		}
		this.type = 'expense';

		this.classifications = [{
			id: this.classificationForm.value.id,
			type: this.type,
			label: this.classificationForm.value.label,
			description: null,
			parentId: null,
		}]

		this.service.updateAll(ApiUrl.configClassification, this.classifications).
			subscribe(res => {
				this.loading = false;
				this.translate.get('parametresCompatibility.form').subscribe(text => {

					toastr.success(text.addd.successClassification, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
				this.modalClassificationState = false;
				this.classificationForm.reset();
				this.getExpense();
			}, err => {
				this.loading = false;

				this.translate.get('parametresCompatibility.form').subscribe(text => {
					toastr.error(text.addd.errorClassification, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
			})
	}


	/** 
	* Init form sub classification
	**/
	async initSubClassificationForm() {
		const categoriesDepense = (await this.service.getAll(ApiUrl.configCategory).toPromise()).value;
		this.subCategoryDepense = categoriesDepense.filter(x => x.type === 1)[0].subCategories;

		this.subClassificationForm = this.formBuilder.group({
			id: [null],
			label: ['', [Validators.required]],
			chartAccountItemId: ['', [Validators.required]],
			description: ['']
		});
	}

	/** 
	 * On sub classfification add
	 * @param e The event object of the statut filter
	 * @param index The sub classification index
	 **/
	onSubClassificationAdd(e: MouseEvent, index: number) {
		e.stopPropagation();
		this.subClassificationForm.reset();
		this.currentClassification = this.classifications[index];
		this.modalSubClassificationState = true;
	}

	/**
	 * On update sub classification
	 * @param index The sub classification index
	 * @param parent The classification index 
   **/
	onSubClassificationUpdate(index: number, parent: number) {

		this.currentClassification = this.classifications[parent];
		const subClassification = this.currentClassification['subClassification'][index];
		this.subClassificationForm.controls['label'].setValue(subClassification.label);
		this.subClassificationForm.controls['id'].setValue(subClassification.id);
		this.subClassificationForm.controls['chartAccountItemId'].setValue(subClassification.chartAccountItem.id);
		this.subClassificationForm.controls['description'].setValue(subClassification.description);
		this.modalSubClassificationState = true;

	}

	/** 
  * On delete sub classification
  * @param index The sub classification index
  * @param parent The classification index
  **/
	subClassificationDelete(index: number, parent: number) {

		const deletedSubClassification = this.classifications[parent]['subClassification'][index];

		/* what that the id you need  */
		this.translate.get('parametresCompatibility.delete').subscribe(text => {
			swal({
				title: text.title,
				text: text.questionSubClassification,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: false
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: false
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.service.delete(ApiUrl.configClassification, deletedSubClassification.id)
						.subscribe(res => {
							if (res) {
								this.getExpense();
								swal(text.success, '', 'success');
							} else {
								swal(text.error, '', 'error');
							}

						});
				} else {
					swal(text.cancel, '', 'error');
				}
			});
		});

	}

	/** 
	* Add  update sub classification
	**/
	changeSubClassification() {
		if (!this.subClassificationForm.valid) {
			return false;
		}

		const subClassifications = [{
			id: this.subClassificationForm.value.id,
			type: this.type,
			label: this.subClassificationForm.value.label,
			description: this.subClassificationForm.value.description,
			chartAccountItemId: this.subClassificationForm.value.chartAccountItemId,
			parentId: this.currentClassification.id,

		}]

		this.service.updateAll(ApiUrl.configClassification, subClassifications).
			subscribe(res => {
				this.loading = false;
				this.translate.get('parametresCompatibility.form').subscribe(text => {
					toastr.success(text.addd.success, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
				this.modalSubClassificationState = false;
				this.subClassificationForm.reset();
				this.getExpense();
			}, err => {
				this.loading = false;
				this.translate.get('parametresCompatibility.form').subscribe(text => {
					toastr.success(text.addd.success, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
			})
	}


	/** 
	* On add expense
	**/
	async onAddExpense() {
		await this.initFormExpense();
		this.modalSubClassificationState = false;
		this.modalExpenseState = true;
	}


	/** 
	* Init form  expense
	**/
	async initFormExpense() {
		this.expenseForm = this.formBuilder.group({
			label: ['', [Validators.required]],
			code: ['', [Validators.required]],
		});
	}

	/**
	 * Add expense from classification expense
	 * **/
	addExpense() {
		const resCatgorie = {
			'id': '',
			'type': 1,
			'categoryType': 0,
			'label': this.expenseForm.value.label,
			'code': this.expenseForm.value.code,
			'description': '',
			'vatValue': 0,
			'parentId': 2,
			'subCategories': []
		}

		this.service.updateAll(ApiUrl.configCategory, [resCatgorie])
			.subscribe(res => {

				this.translate.get('parametrageplanComptable').subscribe(text => {
					toastr.success(text.msgUpdate, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
				this.initSubClassificationForm();
				this.expenseForm.reset();
				this.modalSubClassificationState = true;
				this.modalExpenseState = false;
			});
	}

}
