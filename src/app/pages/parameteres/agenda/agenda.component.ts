import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { MissionKind } from 'app/Enums/missionKinds';
import { HeaderService } from 'app/services/header/header.service';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';

declare var toastr: any;
declare var jQuery: any;

export enum MissionType {

	technician_task_types,
	appointment_types,
	call_types,
}

@Component({
	selector: 'app-agenda',
	templateUrl: './agenda.component.html',
	styleUrls: ['./agenda.component.scss']
})
export class AgendaComponent implements OnInit {


	MissionKind: typeof MissionKind = MissionKind;
	selectedTab = MissionType.technician_task_types;
	loading = false;
	processing = false;
	ApiUrl: typeof ApiUrl = ApiUrl;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private header: HeaderService,
		private translate: TranslateService
	) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.header.breadcrumb = [
			{ label: this.translate.instant('typeAgenda.title'), route: '/parameteres/agenda' }
		];
	}


	tabs(e) {
		this.selectedTab = +(e.tab.content.viewContainerRef.element.nativeElement.dataset.name);
	}

}
