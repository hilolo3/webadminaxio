import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AppSettings } from 'app/app-settings/app-settings';
import { TranslateService } from '@ngx-translate/core';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { UserProfile } from 'app/Enums/user-profile.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { GlobalInstances } from 'app/app.module';
import { LocalElements } from 'app/shared/utils/local-elements';
import { EditDocumentHelper } from 'app/libraries/document-helper';
import { Action } from 'app/Enums/action';
import { User } from 'app/Models/Entities/User';
declare var toastr: any;

@Component({
	selector: 'app-add',
	templateUrl: './add.component.html',
	styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

	public form: FormGroup;
	public Profiles = [];
	public ProfilesTechnicien = [];
	role;
	docType = '';
	id;
	user: User;

	public typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	userProfile: typeof UserProfile = UserProfile;
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private fb: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
		private translate: TranslateService
	) {
		//localStorage.setItem(LocalElements.docType, ApiUrl.User);
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);


		this.getRole();
		EditDocumentHelper.typeAction = EditDocumentHelper.getTypeAction();
		this.createForm();
	}

	get f() { return this.form.controls; }
	createForm() {
		this.form = this.fb.group({
			nom: ['', [Validators.minLength(2), Validators.required]],
			prenom: ['', [Validators.minLength(2), Validators.required]],
			actif: [true],
			email: [null, [Validators.pattern(AppSettings.regexEmail)]],
			phonenumber: ['', [Validators.minLength(10), Validators.pattern(AppSettings.regexPhone)]],
			username: ['', [Validators.minLength(6), Validators.required], this.checkUniqueUserName.bind(this)],
			password: ['', [Validators.minLength(6)]],
			confirmpassword: ['', [Validators.minLength(6)]],
			idprofile: ['', Validators.required],
			matricule: [null, [Validators.required], this.CheckUniqueIsReference.bind(this)],
			typeTechnicien: [null]
		}, { validator: this.checkPasswords });

		if (!this.isModif()) {
			this.form.get('password').setValidators([Validators.required]);
			this.form.get('confirmpassword').setValidators([Validators.required]);
			this.generateReference();
		}

		this.form.get('idprofile').valueChanges.subscribe(data => {
			if (data === this.getTechnicien()) {
				this.form.get('typeTechnicien').setValidators([Validators.required]);
			}
		});
	}
	checkPasswords(group: FormGroup) {
		const password = group.controls.password.value;
		const confirmPassword = group.controls.confirmpassword.value;
		return password === confirmPassword ? null : { notSame: true }
	}


	checkUniqueUserName(control: FormControl) {
		if (control.value !== '') {
			const promise = new Promise((resolve, reject) => {
				this.service
					.getById(ApiUrl.User + ApiUrl.CheckUserName, control.value)
					.subscribe(res => {

						if (!res && this.isModif() && this.form.value.username !== this.user.userName) {
							resolve({ checkUniqueUserName: true });
						} else if (!res && !this.isModif()) {
							resolve({ checkUniqueUserName: true });
						} else {
							resolve(null);
						}
					});
			});
			return promise;
		}
	}

	ngOnInit() {
		this.docType = localStorage.getItem(LocalElements.docType);
		ManageDataTable.docType = this.docType;
		this.translate.get('...').toPromise().then(async () => {
			if (this.role !== undefined) { this.getProfiles() };
			if (this.isModif()) {
				this.route.params.subscribe(params => {
					this.id = params['id'];
					this.service.getById(ApiUrl.User, this.id).subscribe(value => {
						this.user = value.value;
						this.SetData();
					});
				});
			}
		});
	}

	SetData() {
		// this.form.controls['id'].setValue(this.user.id);
		this.form.controls['nom'].setValue(this.user.lastName);
		this.form.controls['prenom'].setValue(this.user.firstName);
		this.form.controls['actif'].setValue(this.user.statut);
		this.form.controls['email'].setValue(this.user.email);
		this.form.controls['phonenumber'].setValue(this.user.phoneNumber);
		this.form.controls['username'].setValue(this.user.userName);
		this.form.controls['matricule'].setValue(this.user.reference);
		if (this.user.role.id !== this.getTechnicien() && this.user.role.id !== this.getTechnicienChantier()
			&& this.user.role.id !== this.getTechnicienMaintenance()) {
			this.form.controls['idprofile'].setValue(this.user.role.id);
		} else {
			this.form.controls['idprofile'].setValue(this.getTechnicien());
			this.form.controls['typeTechnicien'].setValue(this.user.role.id);
		}
		this.form.get('idprofile').valueChanges.subscribe(data => {
			if (data === this.getTechnicien()) {
				this.form.get('typeTechnicien').setValidators([Validators.required]);
			}
		});
	}

	getTechnicienChantier() {
		if (this.role !== undefined) {
			return this.role.find(x => x.type === this.userProfile.technicienChantier).id;
		}

	}

	getTechnicienMaintenance() {
		if (this.role !== undefined) {
			return this.role.find(x => x.type === this.userProfile.technicienMaintenance).id;
		}

	}

	submit() {

		if (EditDocumentHelper.isAction(Action.AJOUTER)) {
			this.add();
		}

		if (EditDocumentHelper.isAction(Action.MODIFIER)) {
			this.update();
		}

	}

	getProfiles() {
		this.translate.get('LABELS').subscribe(labels => {
			this.Profiles.push({ id: this.role.find(x => x.type === this.userProfile.admin).id, libelle: labels.admin });
			this.Profiles.push({ id: this.role.find(x => x.type === this.userProfile.technicien).id, libelle: labels.technicien });
			this.Profiles.push({ id: this.role.find(x => x.type === this.userProfile.manager).id, libelle: labels.manager });
			this.ProfilesTechnicien.push({ id: this.role.find(x => x.type === this.userProfile.technicien).id, libelle: labels.technicien })
			this.ProfilesTechnicien.push({
				id: this.role.find(x => x.type === this.userProfile.technicienChantier).id,
				libelle: labels.technicienChantier
			});
			this.ProfilesTechnicien.push({
				id: this.role.find(x => x.type === this.userProfile.technicienMaintenance).id,
				libelle: labels.technicienmaintenace
			});
		});
	}

	getTechnicien() {
		if (this.role !== undefined) {
			const test = this.role.find(x => x.type === this.userProfile.technicien);
			return test !== undefined ? test.id : null;
		}
	}

	getRole() {
		this.service.getAll(ApiUrl.role).subscribe(res => {
			this.role = res.value;
			this.getProfiles();
		});
	}

	add() {

		if (this.form.valid) {

			const values = this.form.value;

			values.idprofile = values.idprofile === this.getTechnicien() ? values.typeTechnicien : values.idprofile;
			delete values.typeTechnicien;
			values['actif'] = this.f.actif.value ? true : false;

			this.service.create(ApiUrl.User + ACTION_API.create, this.setData(values)).subscribe(res => {
				if (res.isSuccess) {
					const success = this.translate.instant('toast.add-sucsess')
					toastr.success(success, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					this.router.navigate(['/utilisateurs/detail', res.value.id]);
				}
			}, err => {
				const errors = this.translate.instant('errors.emailExist')
				toastr.warning(errors, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});

		} else {
			const errors = this.translate.instant('errors.fillAll')
			toastr.warning(errors, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		}
	}


	update() {
		if (this.form.valid) {
			const values = this.form.value;
			values.idprofile = values.typeTechnicien != null &&
				values.idprofile === this.getTechnicien() ? values.typeTechnicien : values.idprofile;
			values['actif'] = values['actif'] ? true : false;
			const res = this.setData(values);
			this.service.update(ApiUrl.User, res, this.id).subscribe(result => {
				if (res) {
					const text = this.translate.instant('toast.update-sucsess')
					toastr.success(text, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					this.router.navigate(['/utilisateurs/detail', result.value.id]);
				}
			});

		} else {
			const errors = this.translate.instant('errors.fillAll')
			toastr.warning(errors, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		}
	}


	CheckUniqueIsReference(control: FormControl): Promise<any | null> {
		if (control.value !== '') {
			const promise = new Promise((resolve, reject) => {
				this.service
					.getById(ApiUrl.User + ApiUrl.CheckReference, control.value)
					.subscribe(res => {
						if (!res && this.isModif() && this.form.value.matricule !== this.user.reference) {
							resolve({ CheckUniqueReference: true });
						} else if (!res && !this.isModif()) {
							resolve({ CheckUniqueReference: true });
						} else {
							resolve(null);
						}
					});
			});
			return promise;
		}
	}


	generateReference() {
		this.service.getAll(ApiUrl.configReference + this.typeNumerotation.agent + '/reference')
			.subscribe(res => {
				this.form.controls['matricule'].setValue(res);
			})
	}


	setData(form) {
		return {
			'firstName': form.prenom,
			'lastName': form.nom,
			'reference': form.matricule,
			'password': form.password,
			'userName': form.username,
			'statut': form.actif,
			'email': form.email,
			'phoneNumber': form.phonenumber,
			'roleId': form.idprofile
		}
	}

	close() {
		ManageDataTable.docType = this.docType;
		GlobalInstances.router.navigateByUrl(ManageDataTable.getListRoute());
	}

	isModif() { return EditDocumentHelper.isAction(Action.MODIFIER) }



}
