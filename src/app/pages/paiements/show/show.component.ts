import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { AppSettings } from 'app/app-settings/app-settings';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { Payment } from 'app/Models/Entities/Documents/Payments';
import { HeaderService } from 'app/services/header/header.service';
import { LocalElements } from 'app/shared/utils/local-elements';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { TranslateConfiguration } from 'app/libraries/translation';

@Component({
	selector: 'app-show',
	templateUrl: './show.component.html',
	styleUrls: ['./show.component.scss']
})
export class ShowComponent implements OnInit {

	id
	paiement: Payment;
	historique
	selectedTabs = 'information';
	docType = '';

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate?: TranslateService,
		private route?: ActivatedRoute,
		private header?: HeaderService,

	) {
		TranslateConfiguration.setCurrentLanguage(this.translate)
	}

	ngOnInit() {
		this.docType = localStorage.getItem(LocalElements.docType);
		ManageDataTable.docType = this.docType;
		this.route.params.subscribe(params => {
			this.id = params['id'];
			this.GetPaiementById(this.id);
		});
	}

	GetPaiementById(id) {
		this.service.getById(ApiUrl.Payment, id).subscribe(res => {
			this.paiement = res.value;
			this.prepareBreadcrumb();
			this.historique = res.value.changesHistory;
		})
	}
	prepareBreadcrumb(): void {
		this.translate.get(`doctype.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('doctype.' + ManageDataTable.docType), route: ManageDataTable.getListRoute() },
				{ label: this.paiement.description, route: ManageDataTable.getRouteDetails(this.paiement) }
			];
		});
	}
	tabs(e) {
		this.selectedTabs = e.tab.content.viewContainerRef.element.nativeElement.dataset.name;
	}

}
