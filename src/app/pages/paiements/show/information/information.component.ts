import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { TypePaiement } from 'app/Enums/TypePaiement.Enum';
import { TypeDocument } from 'app/Enums/TypeDocument.enums';
import { Payment } from 'app/Models/Entities/Documents/Payments';

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'information-paiements',
	templateUrl: './information.component.html',
	styleUrls: ['./information.component.scss']
})
export class InformationComponent implements OnInit, OnChanges {

	@Input() paiement: Payment;
	typePaiement: typeof TypePaiement = TypePaiement;
	typeDocument: typeof TypeDocument = TypeDocument;
	processing = false;
	constructor(
		private translate: TranslateService
	) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
	}
	ngOnChanges() {
	}

	routerLink(typeDoc) {

		let url = ''
		switch (typeDoc) {
			case TypeDocument.Invoice:
				url = 'factures';
				break;
			case TypeDocument.Expenses:
				url = 'depense';
				break;
			case TypeDocument.CreditNote:
				url = 'avoirs';
				break;
		}

		return url;
	}
	getOperation(operation) {
		switch (operation) {
			case this.typePaiement.Payment:
				return this.translate.instant('labels.paiement');
				break;
			case this.typePaiement.GroupedPayment:
				return this.translate.instant('labels.paiementGroupe');
				break;
			case this.typePaiement.Transfer_From:
				return this.translate.instant('labels.virement');
				break;
			case this.typePaiement.Transfer_To:
				return this.translate.instant('labels.recevoir');
				break;
			case this.typePaiement.CreditNotePayment:
				return this.translate.instant('labels.parAvoir');
				break;
		}
	}
}
