import { Component, OnInit, EventEmitter, Output, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AppSettings } from 'app/app-settings/app-settings';
import { ParametrageCompte } from 'app/Models/Entities/Parametres/ParametrageCompte';
import { TranslateService } from '@ngx-translate/core';
import { Historique } from 'app/Models/Entities/Commun/Historique';
import { Modereglement } from 'app/Models/Entities/Parametres/Modereglement';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { StaticModeReglement } from 'app/Enums/StaticModeReglement.Enum';
import { Typepaiement } from 'app/components/paiement-facture/paiement.component';
import { TypePaiement } from 'app/Enums/TypePaiement.Enum';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


declare var toastr: any;
declare var jQuery: any;

@Component({
	selector: 'mouvement-compte',
	templateUrl: './mouvement-compte.component.html',
	styleUrls: ['./mouvement-compte.component.scss']
})
export class MouvementCompteComponent implements OnInit {

	form
	comptes: ParametrageCompte[] = null
	loading = false;
	modesRegelement: Modereglement[] = null;
	dateLang
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private fb: FormBuilder,
		private translate: TranslateService,
		public dialogRef: MatDialogRef<MouvementCompteComponent>,
		@Inject(MAT_DIALOG_DATA) public data,
	) {
		this.form = this.fb.group({
			'montant': [null, [Validators.required]],
			'idCompteDebit': [null, [Validators.required]],
			'idCompteCredit': [null, [Validators.required]],
			'datePaiement': [new Date(), [Validators.required]],
			'idModePaiement': [null, [Validators.required]],
		})
	}

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.translate.get('datePicker').subscribe(text => {
			this.dateLang = text;
		});
		this.GetCompte('');

		this.GetModeRegelement('')
	}

	// Récuperer la liste des modes régelements
	GetModeRegelement(search) {
		if (this.modesRegelement == null) {
			this.service.getAll(ApiUrl.configModeRegelemnt)
				.subscribe(
					res => {
						this.modesRegelement = res.value.filter(x => x.id != StaticModeReglement.Avoir)
					}
				)
		}
	}

	// Récuperer la liste des comptes
	GetCompte(search) {
		if (this.comptes == null) {
			this.service.getAll(ApiUrl.configCompte)
				.subscribe(res => {
					this.comptes = res.value
				})
		}
	}
	close() { this.dialogRef.close(); }

	// Get information de form
	get f() { return this.form.controls; }

	saveMovement() {
		if (this.form.valid) {
			const values = this.form.value;

			const res = {
				'transferAmount': values['montant'],
				'creditedAccountId': values['idCompteCredit'],
				'debitedAccountId': values['idCompteDebit'],
				// tslint:disable-next-line: radix
				'paymentMethodId': parseInt(values['idModePaiement'])
			}
			if (values['idCompteDebit'] === values['idCompteCredit']) {
				this.translate.get('errors').subscribe(text => {
					toastr.warning(text.memeCompte, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
			} else if (values['montant'] <= 0) {
				this.translate.get('errors').subscribe(text => {
					toastr.warning(text.montantNegative, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
			} else {
				this.loading = true;
				this.service.create(ApiUrl.Payment + '/Create/Transfer', res)
					.subscribe((res: any) => {
						this.loading = false;
						if (res) {
							this.translate.get('list').subscribe(text => {
								this.form.reset();
								this.form.controls['datePaiement'].setValue(new Date());
								// jQuery('#mouvementCompte').modal('hide')
								// this.refresh.emit('')
								this.dialogRef.close(res);
							})
						}
					}, err => {
						this.loading = false;
						this.translate.get('errors').subscribe(text => {
							toastr.warning(text.errorServer, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
						});
					})
			}
		} else {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		}
	}

}
