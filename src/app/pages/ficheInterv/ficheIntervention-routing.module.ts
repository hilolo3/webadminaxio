import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditFicheInterventionComponent } from './details-ficheIntervention/edit-ficheIntervention/edit-ficheIntervention.component';
import { IndexFIComponent } from './index/index.component';

export const routes: Routes = [
	{
		path: '',
		children: [
			{
				path: '',
				pathMatch: 'full',
				component : IndexFIComponent
			},
			{
				path: 'detail/:id',
				loadChildren: './details-ficheIntervention/details-ficheIntervention.module#DetailsFicheInterventionModule'
			},
			{
				path: 'create',
				component : EditFicheInterventionComponent
			},
			{
				path: 'generer-create/:id',
				component : EditFicheInterventionComponent
			},
			{
				path: 'dupliquer-create/:id',
				component : EditFicheInterventionComponent
			},
			{
				path: 'edit/:id',
				component: EditFicheInterventionComponent
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class FicheInterventionRoutingModule { }
