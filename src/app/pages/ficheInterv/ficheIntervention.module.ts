import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { PreviousRouteService } from 'app/services/previous-route/previous-route.service';
import { FicheInterventionRoutingModule } from './ficheIntervention-routing.module';
import { MatTabsModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatIconModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { EditFicheInterventionComponent } from './details-ficheIntervention/edit-ficheIntervention/edit-ficheIntervention.component';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { CommonModules } from 'app/common/common.module';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { SplitButtonModule, CalendarModule } from 'app/custom-module/primeng/primeng';
import { SelectUserModule } from 'app/common/select-user/select-user.module';
import { IndexFIComponent } from './index/index.component';
import { ScheduleAllModule } from '@syncfusion/ej2-angular-schedule';
import { ListIndexModule } from 'app/components/form-data/index-list/index.module';
import { InputTextareaModule } from 'app/common/input-textarea/input-textarea.module';
import { TableArticleModule } from 'app/common/table-article/table-article.module';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { AddChantierFormModule } from 'app/common/chantier-form/add-chantier-form.module';
import {
	NgxMatDatetimePickerModule,
	NgxMatNativeDateModule,
	NgxMatTimepickerModule
} from '@angular-material-components/datetime-picker';


export function MultiLoader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
		{ prefix: './assets/i18n/status/', suffix: '.json' },
		{ prefix: './assets/i18n/chantier/', suffix: '.json' },
		{ prefix: './assets/i18n/ficheintervention/', suffix: '.json' },
		{ prefix: './assets/i18n/ficheinterventionmaintenance/', suffix: '.json' },
		{ prefix: './assets/i18n/prestation/', suffix: '.json' },

	]);
}


@NgModule({
	providers: [
		PreviousRouteService],
	imports: [
		CommonModule,
		FormsModule,
		FicheInterventionRoutingModule,
		SplitButtonModule,
		SelectUserModule,
		MatIconModule,
		NgxMatSelectSearchModule,
		MatTabsModule,
		NgxMatDatetimePickerModule,
		NgxMatTimepickerModule,
		NgxMatNativeDateModule,
		MatDatepickerModule,
		AddChantierFormModule,
		InputTextareaModule,
		ReactiveFormsModule,
		TableArticleModule,
		MatSelectModule,
		MatNativeDateModule,
		NgbTooltipModule,
		CommonModules,
		ListIndexModule,
		ScheduleAllModule,
		CalendarModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: MultiLoader,
				deps: [HttpClient]
			},
			isolate: true
		}),
	],
	exports: [EditFicheInterventionComponent, IndexFIComponent],
	declarations: [EditFicheInterventionComponent, IndexFIComponent]
})

export class FicheInterventionModule { }

