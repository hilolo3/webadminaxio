import { Component, OnInit, Inject } from '@angular/core';
import { DevisDetail } from 'app/Models/Entities/Dashboard/DashbordVente';
import { TranslateService } from '@ngx-translate/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppSettings } from 'app/app-settings/app-settings';

@Component({
	selector: 'app-list-devis',
	templateUrl: './list-devis.component.html',
	styleUrls: ['./list-devis.component.scss']
})
export class ListDevisComponent implements OnInit {
	devisDetails: DevisDetail[];

	constructor(
		private translate: TranslateService,

		public dialogRef: MatDialogRef<ListDevisComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { listes: DevisDetail[] }

	) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
	}

	ngOnInit() {
		this.devisDetails = this.data.listes;
	}

	fermer() {
		this.dialogRef.close();
	}

}
