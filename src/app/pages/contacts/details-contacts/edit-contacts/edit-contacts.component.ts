// #region imports
import { Component, OnInit, OnDestroy, AfterViewInit, Inject, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { LocalElements } from 'app/shared/utils/local-elements';
import { DatePipe } from '@angular/common';
import { EditDocumentHelper } from 'app/libraries/document-helper';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { AppSettings } from 'app/app-settings/app-settings';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import * as _ from 'lodash';
import { Subscription, of } from 'rxjs';
import { filter, debounceTime } from 'rxjs/operators';
import { Action } from 'app/Enums/action';
import { ClientType } from 'app/Enums/TypeClient.enum';
import { Groupe } from 'app/Models/Entities/Contacts/Groupe';
import { Contact } from 'app/Models/Entities/Commun/Contact';
import { Adresse } from 'app/Models/Entities/Commun/Adresse';
import { GlobalInstances } from 'app/app.module';
import { ManageDataTable } from 'app/libraries/manage-data-table';

declare var toastr: any;
declare var jQuery: any;


// #endregion


@Component({
	selector: 'app-fiche-contacts',
	templateUrl: './edit-contacts.component.html',
	styleUrls: [
		'./edit-contacts.component.scss',
	],
	providers: [DatePipe]
})


export class EditContactsComponent implements OnInit, OnDestroy, AfterViewInit {
	form;
	docType = '';
	id = '';
	subscriptions: Subscription[] = [];
	document;
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	clientType: typeof ClientType = ClientType;
	groupes: Groupe[] = [];
	CodeComptable;
	contacts: Contact[] = [];
	adresses: Adresse[] = [];
	processing = false;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		public router: Router,
		private route: ActivatedRoute,
		private fb: FormBuilder,
		public formBuilder: FormBuilder,
		public translate: TranslateService,
		public activatedRoute: ActivatedRoute,
	) {
		this.docType = localStorage.getItem(LocalElements.docType);
		EditDocumentHelper.typeAction = EditDocumentHelper.getTypeAction();
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.getGroupes();
		this.createForm();

		this.subscriptions.push(
			this.router.events.pipe(
				debounceTime(0),
				filter(e => e instanceof NavigationEnd))
				.subscribe(async () => {
					if (
						EditDocumentHelper.isAction(Action.AJOUTER)) {

					}
					if (
						EditDocumentHelper.isAction(Action.MODIFIER)) {
						this.id = this.activatedRoute.snapshot.params.id;

						await this.getDocument();
						this.setData();

					}
				})
		);
	}

	async ngOnInit() {
		this.CodeComptable = await this.GetCodeComptable();
		await this.initializeCreationForm();
	}

	ngAfterViewInit(): void {
		// init helper
	}

	ngOnDestroy(): void {

	}

	createForm() {
		this.form = this.fb.group({
			firstName: [null, [Validators.required]],
			lastName: [''],
			reference: [null, [Validators.required], this.CheckUniqueReference.bind(this)],
			code: [null],
			phoneNumber: [null, [Validators.minLength(10), Validators.pattern(AppSettings.regexPhone)]],
			landLine: [null, [Validators.pattern(AppSettings.regexPhone)]],
			email: [null, [Validators.pattern(AppSettings.regexEmail)]],
			website: [null, [Validators.pattern(AppSettings.regexURL)]],
			siret: [null],
			intraCommunityVAT: [null],
			accountingCode: [null],
			groupeId: [null],
			type: [this.clientType.Particular, [Validators.required]],
			note: [null],
			paymentCondition: [null],
			street: [null],
			complement: [null],
			city: [null],
			postalCode: [null],
			department: [null],
			countryCode: [null],

		});

		if (this.isFournisseur()) {
			this.form.get('street').setValidators([Validators.required]);
			this.form.get('city').setValidators([Validators.required]);
			this.form.get('postalCode').setValidators([Validators.required]);
		}

	}

	CheckUniqueReference(control: FormControl) {
		if (control.value !== '') {
			const type = this.isClient() ? ApiUrl.Client : ApiUrl.Fournisseur;
			const promise = new Promise((resolve, reject) => {
				this.service
					.getById(type + ApiUrl.CheckReference, control.value)
					.subscribe(res => {
						if (!res && this.isModif() && this.form.value.reference !== this.document.reference) {
							resolve({ CheckUniqueReference: true });
						} else if (!res && !this.isModif()) {
							resolve({ CheckUniqueReference: true });
						} else {
							resolve(null);
						}
					});
			});
			return promise;
		}
	}

	async initializeCreationForm() {
		const code = await this.generatecode();
		this.form.controls['reference'].setValue(code);
		this.form.controls['accountingCode'].setValue(this.CodeComptable);
	}

	get f() { return this.form.controls; }

	generatecode(): Promise<string> {
		const type = this.isClient() ? this.typeNumerotation.Client : this.typeNumerotation.fournisseur
		return new Promise((reslove, reject) => {
			this.service.getAll(ApiUrl.configReference + type + '/reference').subscribe(
				res => {
					reslove(res);
				},
				err => {
					console.log(err);
				}
			);
		});
	}

	isClient() { return this.docType === ApiUrl.Client }

	isFournisseur() { return this.docType === ApiUrl.Fournisseur }

	setData() {

		this.form.controls['note'].setValue(this.document.note);
		this.form.controls['paymentCondition'].setValue(this.document.paymentCondition);
		this.form.controls['reference'].setValue(this.document.reference);
		this.form.controls['code'].setValue(this.document.code);
		this.form.controls['firstName'].setValue(this.document.firstName);
		this.form.controls['phoneNumber'].setValue(this.document.phoneNumber);
		this.form.controls['landLine'].setValue(this.document.landLine);
		this.form.controls['email'].setValue(this.document.email);
		this.form.controls['website'].setValue(this.document.website);
		this.form.controls['siret'].setValue(this.document.siret);
		this.form.controls['intraCommunityVAT'].setValue(this.document.intraCommunityVAT);
		this.form.controls['accountingCode'].setValue(this.document.accountingCode);

		this.contacts = this.document.contactInformations as Contact[];

		if (this.isClient()) {
			this.form.controls['lastName'].setValue(this.document.lastName);
			this.form.controls['type'].setValue(this.document.type);
			this.form.controls['groupeId'].setValue(this.document.groupeId != null ? this.document.groupeId.toString() : null);
			this.adresses = this.document.addresses as Adresse[];
		}
		if (this.isFournisseur()) {
			this.form.controls['street'].setValue(this.document.address.street);
			this.form.controls['complement'].setValue(this.document.address.complement);
			this.form.controls['city'].setValue(this.document.address.city);
			this.form.controls['postalCode'].setValue(this.document.address.postalCode);
			this.form.controls['department'].setValue(this.document.address.department);
			this.form.controls['countryCode'].setValue(this.document.address.countryCode);
		}
	}


	getDocument(): Promise<any> {
		return new Promise((resolve, reject) => {
			if (this.id && this.id !== '') {

				this.service.getById(this.docType, this.id).subscribe(res => {
					this.document = res.value;
					resolve(res.value);
				});
			}
		});
	}


	getGroupes() {
		this.service.getAll(ApiUrl.Groupe).subscribe(res => {
			this.groupes = res.value;

		}, err => {
			console.log(err);
		})
	}

	GetCodeComptable(): Promise<number> {
		return new Promise((resolve, reject) => {
			this.service.getAll(ApiUrl.configCategory).subscribe(
				res => {
					resolve(res.value.find(x => + x.id === 11)['code']);
				},
				err => {
					reject(err);
				}
			);
		});
	}


	//#region adresse
	getListAdresses(adresses) {
		this.adresses = adresses;
	}
	removeAdresse(i) {
		this.adresses.splice(i, 1);
	}
	//#endregion

	//#region contact
	getListContacts(contacts) {
		this.contacts = contacts;
	}

	removeContact(event) {
		this.contacts.splice(event.contactIndex, 1);
	}
	//#endregion


	generateCodeComptable() {
		this.form.controls['accountingCode'].setValue(this.CodeComptable + this.f.firstName.value.replace(/ /g, ''));
	}
	isModif() { return EditDocumentHelper.isAction(Action.MODIFIER); }

	setAdresse() {
		return {
			'designation': '',
			'department': this.form.controls['department'].value,
			'street': this.form.controls['street'].value,
			'complement': this.form.controls['complement'].value,
			'city': this.form.controls['city'].value,
			'postalCode': this.form.controls['postalCode'].value,
			'countryCode': this.form.controls['countryCode'].value,
			'isDefault': true
		}
	}

	add() {
		this.processing = true;
		if (this.isClient() && this.adresses.length === 0) {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.adresseRequired, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
			this.processing = false;
			return
		}
		if (this.isClient() && this.adresses.filter(x => x.isDefault).length === 0) {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.adresseDefaultRequired, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
			this.processing = false;
			return
		}
		if (this.form.valid) {
			let values = this.form.value;
			values = AppSettings.ConvertEmptyValueToNull(values);
			if (this.isClient()) { values['addresses'] = this.adresses; }
			values['contactInformations'] = this.contacts;
			if (this.isFournisseur()) { values['address'] = this.setAdresse(); }
			const type = this.isClient() ? ApiUrl.Client : ApiUrl.Fournisseur;
			this.service.create(type + ACTION_API.create, values).subscribe(res => {
				if (res) {
					this.processing = false;
					this.toast('add-sucsess');
					if (this.isFournisseur()) { this.router.navigate(['/fournisseurs/detail', res.value.id]); }
					if (this.isClient()) { this.router.navigate(['/clients/detail', res.value.id]); }
				}
			});
		} else {
			this.translate.get('errors').subscribe(text => {

				toastr.warning(text.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		}
	}

	update() {
		this.processing = true;
		if (this.isClient() &&  this.adresses.length === 0) {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.adresseRequired, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
			this.processing = false;
			return
		}
		if (this.isClient() &&  this.adresses.filter(x => x.isDefault).length === 0) {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.adresseDefaultRequired, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
			this.processing = false;
			return
		}
		if (this.form.valid) {
			const listAdresse = [];
			this.adresses.forEach(eleme => {
				listAdresse.push(eleme.isDefault === null ? this.SetDefault(eleme) : eleme);

			})
				let values = this.form.value;
				values = AppSettings.ConvertEmptyValueToNull(values);
				values['contactInformations'] = this.contacts as Contact[];
				if (this.isClient()) { values['addresses'] = listAdresse; }
				if (this.isFournisseur()) { values['address'] = this.setAdresse(); }
				const type = this.isClient() ? ApiUrl.Client : ApiUrl.Fournisseur;
				this.service.update(type, values, this.id).subscribe(
					res => {
						if (res) {
							this.toast('update-sucsess');
							if (this.isFournisseur()) { this.router.navigate(['/fournisseurs/detail', res.value.id]); }
							if (this.isClient()) { this.router.navigate(['/clients/detail', res.value.id]); }
							this.processing = false;
						}
					}
				);
		} else {
			this.processing = false;
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		}
	}

	SetDefault(ele) {
		ele.isDefault = false;
		return ele
	}


	toast(msg) {
		return toastr.success(this.translate.instant('toast.' + msg), '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
	}

	close() {
		ManageDataTable.docType = this.docType;
		GlobalInstances.router.navigateByUrl(ManageDataTable.getListRoute());
	}

	submit() {

		if (EditDocumentHelper.isAction(Action.AJOUTER)) {
			this.add();
		}

		if (EditDocumentHelper.isAction(Action.MODIFIER)) {
			this.update();
		}

	}


}
