import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LocalElements } from 'app/shared/utils/local-elements';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { AppSettings } from 'app/app-settings/app-settings';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { MatDialog } from '@angular/material';
import { Memo } from 'app/Models/Entities/Commun/Memo';
import { HeaderService } from 'app/services/header/header.service';
import { TranslateConfiguration } from 'app/libraries/translation';
declare var toastr: any;

@Component({
	selector: 'app-details-contacts',
	templateUrl: './details-contacts.component.html',
	styleUrls: [
		'./details-contacts.component.scss',
	]
})
export class DetailsContactsComponent implements OnInit {

	docType = '';
	apiUrl: typeof ApiUrl = ApiUrl;
	selectedTabs = 'information';
	id = '';
	document;
	historique = [];
	memos = [];
	idChantier: number = null;
	processing = false;
	items = [];

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private router: Router,
		private dialog: MatDialog,
		public translate: TranslateService,
		public route: ActivatedRoute,
		public header: HeaderService
	) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
		this.docType = localStorage.getItem(LocalElements.docType);
		ManageDataTable.service = service;
		ManageDataTable.docType = this.docType;
		ManageDataTable.dialog = this.dialog;

	}

	async ngOnInit(): Promise<void> {
		this.id = this.route.snapshot.params.id;
		await this.getDocument();
		this.getItems();

	}


	prepareBreadcrumb(): void {
		this.translate.get(`doctype.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('doctype.' + ManageDataTable.docType), route: ManageDataTable.getListRoute() },
				{ label: this.document.reference, route: ManageDataTable.getRouteDetails(this.document) }
			];
		});
	}

	tabs(e) {
		this.selectedTabs = e.tab.content.viewContainerRef.element.nativeElement.dataset.name;
	}

	async saveMemo(memo: Memo) {
		// if (memo.attachments.length === 0) {
		// 	toastr.warning('Ajouter une fichier', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		// 	return
		// }
		this.processing = true;
		memo['id'] = AppSettings.guid();
		this.service.create(ApiUrl.File + '/' + this.id + '/Create/Memo', memo)
			.subscribe(res => {
				this.processing = false;
				this.getDocument();
			}, err => { console.log(err); this.processing = false; });
	}

	downloadPieceJointe(event) {
		const pieceJointe = event;
		this.service.getById(ApiUrl.File, pieceJointe.fileId).subscribe(
			value => {
				AppSettings.downloadBase64(value.value, pieceJointe.fileName,
					value.value.substring('data:'.length, value.value.indexOf(';base64')),
					pieceJointe.fileType)
			}
		)
	}



	getDocument(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.getById(this.docType, this.id).subscribe(res => {
				this.document = res.value;
				this.prepareBreadcrumb();

				this.historique = this.document.changesHistory;
				this.memos = this.document.memos;
				resolve(res.value);
			});
		});
	}

	getItems() {
		this.items = ManageDataTable.getActions(true);
	}


	modifier() {
		ManageDataTable.getUpdate(this.document);
	}

	isClient() { return this.docType === ApiUrl.Client }

	isFournisseur() { return this.docType === ApiUrl.Fournisseur }


}
