// #region imports
import { Component, OnInit, Inject, Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { LocalElements } from 'app/shared/utils/local-elements';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { ClientType } from 'app/Enums/TypeClient.enum';


// #endregion


@Component({
	selector: 'app-fiche-contacts',
	templateUrl: './fiche-contacts.component.html',
	styleUrls: [
		'./fiche-contacts.component.scss',
	],
	providers: [DatePipe]
})


export class FicheContactsComponent implements OnInit {

	@Input() data;
	docType = '';
	clientType: typeof ClientType = ClientType;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		public router: Router,
		public formBuilder: FormBuilder,
		public translate: TranslateService,
		public activatedRoute: ActivatedRoute,
	) {
		this.docType = localStorage.getItem(LocalElements.docType);
	}

	ngOnInit(): void {
	}

	isClient() { return this.docType === ApiUrl.Client }

	isFournisseur() { return this.docType === ApiUrl.Fournisseur }
}


