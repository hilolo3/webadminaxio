import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
declare var toastr: any;

@Component({
	selector: 'app-change-password',
	templateUrl: './change-password.component.html',
	styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

	public form: any;
	params;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private route: ActivatedRoute,
		private fb: FormBuilder,
		private router: Router) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.form = this.fb.group({
			'password': ['', [Validators.required, Validators.minLength(6)]]
		});
	}

	ngOnInit() {
		this.route.queryParams
			.subscribe(params => {
				this.params = params;
			});
	}

	recover() {
		if (this.form.valid) {
			let res = {
				password: this.form.controls.password.value,
				token: this.params.token,
				userId: this.params.id
			}
			this.service.updateAll(ApiUrl.recetPassword, res).subscribe(res => {
				if (res.isSuccess) {
					this.router.navigate(['/']);
					toastr.success('Le mote de passe a été bien enregistré avec succès', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				} else {
					toastr.warning('external error', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			});
		}
	}

}
