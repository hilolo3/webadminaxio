import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MatDialog, MatDialogConfig } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS } from 'app/shared/dateAdapter/date.adapter';
import { TableArticleComponent } from 'app/common/table-article/table-article.component';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { StatutReccurente } from 'app/Enums/Statut/StatutReccurente.Enum';
import { TypeTermine, PeriodicityRecurringType, PeriodicityType, PeriodicityEndingType } from 'app/Enums/TypeRepetition.Enum';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { Adresse } from 'app/Models/Entities/Commun/Adresse';
import { ReccurenteModel, Reccurente } from 'app/Models/Model/ReccurenteModel';
import { StatutContratEntretien } from 'app/Enums/Statut/StatutContratEntretien.Enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateConfiguration } from 'app/libraries/translation';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
declare var toastr: any;
import * as _ from 'lodash';
import { AppSettings } from 'app/app-settings/app-settings';
import { TypeDocument } from 'app/Enums/TypeDocument.enums';
import { Invoice } from 'app/Models/Entities/Documents/Invoice';
import { TypeFacture } from 'app/Enums/TypeFacture.enum';
import { OrderDetails } from 'app/Models/Entities/Commun/OrderDetails';
import { OrderProductsDetailsType } from 'app/Enums/productsDetailsType.enum';
import { PersonaliseReccurenteComponent } from 'app/common/personalise-reccurente/personalise-reccurente.component';
import { Color } from 'app/Enums/color';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { GlobalInstances } from 'app/app.module';
import { LocalElements } from 'app/shared/utils/local-elements';
import { HeaderService } from 'app/services/header/header.service';
import { DialogHelper } from 'app/libraries/dialog';
import { AddAdresseComponent } from 'app/common/add-adresse/add-adresse.component';
import { AddClientPopComponent } from 'app/components/add-client-pop/add-client-pop.component';
@Component({
	selector: 'app-edit-reccurente',
	templateUrl: './edit-reccurente.component.html',
	styleUrls: ['./edit-reccurente.component.scss'],
	providers: [
		{
			provide: DateAdapter, useClass: AppDateAdapter
		},
		{
			provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
		}
	]
})
export class EditReccurenteComponent implements OnInit {
	@ViewChild(TableArticleComponent) tableArticleComponent: TableArticleComponent;
	id = null;
	reccurent = null;
	form: FormGroup;
	clients = [];
	listClients = [];
	statutReccurente: typeof StatutReccurente = StatutReccurente;
	typeTermineEnum: typeof TypeTermine = TypeTermine;
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	typeRepetitionEnum: typeof PeriodicityRecurringType = PeriodicityRecurringType;
	periodicityType: typeof PeriodicityType = PeriodicityType;
	periodicityEndingType: typeof PeriodicityEndingType = PeriodicityEndingType;
	factureReccurente;
	articles = [];
	chantiers = [];
	listchantiers = [];
	dateLang: any;
	prestations: any;
	emitter: any = {};
	client: Client;
	adresseFacturation: Adresse = new Adresse();
	daysCheckBox: { label: String, value: Boolean, type: number }[];
	adresses: Adresse[] = [];
	anotherArrayadressesIntervention: Adresse[] = [];
	processing: boolean = false;
	newAddress: Adresse[] = [];
	clientfacture;
	typeFacture;
	dataDialog: ReccurenteModel = null;
	result: ReccurenteModel = new ReccurenteModel();
	statutContratEntretien: typeof StatutContratEntretien = StatutContratEntretien;
	mint;
	minFinDate = new Date();
	partProrata = false;
	participationPuc = false;
	retenuGaranti = false;
	isModif = false;
	docType = '';
	color: typeof Color = Color;
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private router: Router,
		private fb: FormBuilder,
		private route: ActivatedRoute,
		private dialog: MatDialog,
		private dateAdapter: DateAdapter<Date>,
		private header: HeaderService,

	) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
		this.dateAdapter.setLocale('fr');
	}

	prepareBreadcrumb(): void {
		this.translate.get(`doctype.`).toPromise().then((translation: string) => {

			this.header.breadcrumb = [
				{ label: this.translate.instant('doctype.' + ManageDataTable.docType), route: ManageDataTable.getListRoute() },
				{ label: this.factureReccurente.document.client.reference, route: ManageDataTable.getRouteDetails(this.factureReccurente) }
			];
		});
	}

	async ngOnInit() {
		this.processing = true;
		this.docType = localStorage.getItem(LocalElements.docType);
		ManageDataTable.docType = this.docType;
		this.header.breadcrumb = [
			{ label: 'Factures Récurrentes', route: ManageDataTable.getListRoute() },
		];
		this.createForm();
		await this.getChantiers('');
		await this.getClients('');
		this.id = await this.getParamsFromRoute('id') as number;
		if (!this.id) {
			this.mint = new Date();
			this.mint.setDate(this.mint.getDate() + 1);
		}
		if (this.id !== undefined) {
			this.isModif = true;
			this.service.getById(ApiUrl.Recurring, this.id).subscribe(res => {
				this.factureReccurente = res.value;
				this.prepareBreadcrumb();
				this.participationPuc = (this.factureReccurente.document['orderDetails']['puc'] > 0);
				this.partProrata = (this.factureReccurente.document['orderDetails']['proportion'] > 0);
				this.retenuGaranti = (this.factureReccurente.document['orderDetails']['holdbackDetails']['holdback'] > 0);
				if (this.factureReccurente.document.workshopId != null) {
					this.LoadchantietClient(this.factureReccurente)
				} else {
					this.getClients('', this.factureReccurente.document.client);
					this.getChantiers('');
				}
				this.initializeData();

			});
		} else {

			await this.getChantiers('');
			await this.getClients('');
		}
		this.processing = false;
	}

	setDate() {
		const date = this.form.controls['date_validation'].value;
		const clonedDate = new Date(date.getTime());
		this.minFinDate = new Date(clonedDate.setDate(clonedDate.getDate() + 1));
	}

	addClient() {
		DialogHelper.openDialog(
			this.dialog,
			AddClientPopComponent,
			DialogHelper.SIZE_LARGE,
			{ docType: ApiUrl.Client }
		).subscribe(async client => {
			if (client !== undefined) {
				await this.getClients('', client);
			}
		});
	}

	addProrata() {
		this.partProrata = !this.partProrata;
	}

	addParticipationPuc() {
		this.participationPuc = !this.participationPuc;
	}

	addRetenuGaranti() {
		this.retenuGaranti = !this.retenuGaranti;
	}

	getParamsFromRoute(paramName: string): Promise<string | number> {
		return new Promise((resolve, reject) => {
			this.route.params.subscribe(params => resolve(params[paramName]))
		});
	}
	//#region  sTATUT
	getColor(status) { return this.color[status] };

	//#endregion
	//#region Form
	createForm() {
		this.form = this.fb.group({
			idChantier: [null],
			client: [null, [Validators.required]],
			adresseFacturation: [null],
			adresseIntervention: [null],
			nbr_repetition: [null, [Validators.required]],
			// type_repetition: [],
			status: [null],
			type_repetition: [1, [Validators.required]],
			dateRepetition: [],
			type_termine: [null, [Validators.required]],
			termine: [],
			date_validation: [null, [Validators.required]],
			date_fin: [],
			nbr_occurence: [0]
		})
	}

	get f() {
		return this.form.controls;
	}
	initializeData() {
		this.form.controls['nbr_repetition'].setValue(this.factureReccurente.periodicityType);
		// tslint:disable-next-line: max-line-length
		this.form.controls['date_validation'].setValue(this.factureReccurente.startingDate !== null ? new Date(this.factureReccurente.startingDate) : null);
		this.form.controls['type_termine'].setValue(this.factureReccurente.endingType);
		this.form.controls['date_fin'].setValue(this.factureReccurente.endingDate !== null ? new Date(this.factureReccurente.endingDate) : null);
		this.form.controls['nbr_occurence'].setValue(this.factureReccurente.occurringCount);
		// this.form.controls['status'].setValue(this.factureReccurente.status.toString());
		this.articles = this.factureReccurente.document.orderDetails.lineItems;

	}
	//#endregion

	//#region fct server
	getChantiers(search, id?: any) {
		this.service.getAll(ApiUrl.Chantier)
			.subscribe(async res => {
				this.processing = true;
				this.chantiers = res.value;
				this.listchantiers = res.value;
				this.form.controls['idChantier'].setValue(id == null ? null : id.toString());
				await this.loadChantierClient(id);
				this.processing = false;
			});
	}


	// Pour récupérer la liste des clients
	getClients(search, cli?: any) {
		this.service.getAll(ApiUrl.Client)
			.subscribe((res) => {

				this.clients = res.value;
				this.listClients = res.value;
				let client: Client;
				// if (this.factureReccurente.idContrat != null) {
				// 	id = this.factureReccurente.contratEntretien.idClient;
				// }
				client = this.getClient(cli);
				if (client === undefined) {
					// this.adresses = [];
					// this.form.controls['client'].setValue(null)
					// this.form.controls['adresseFacturation'].setValue(null)
					// this.form.controls['adresseIntervention'].setValue(null)
					return
				};
				this.form.controls['client'].setValue(client);
				this.form.controls['adresseFacturation'].setValue(client.addresses.find(x => x.isDefault === true));
				this.adresses = client.addresses as Adresse[];
				this.anotherArrayadressesIntervention = client.addresses as Adresse[];
				if (this.factureReccurente) {
					const adresseIntervention = this.factureReccurente.document.addressIntervention
					const index = this.getIndexOfAdress(adresseIntervention)
					this.form.controls['adresseIntervention'].setValue(this.adresses[index]);
				}
				this.processing = false;
			});

	}

	filterAdressInterv(e) {
		if (e !== '') {
			this.adresses = [];
			this.adresses = this.anotherArrayadressesIntervention.filter(x => x.designation.includes(e));
		} else {
			this.adresses = this.anotherArrayadressesIntervention;
		}
	}


	//#endregion

	//#region Chantier

	async loadChantierClient(idChantier) {
		if (this.factureReccurente && this.factureReccurente.maintenanceContract != null) { return; }
		if (idChantier) {
			const chantier = this.chantiers.find(c => c.id === idChantier);
			const idClient = chantier === undefined ? null : chantier.client.id;
			this.form.controls['client'].setValue(chantier !== undefined ? this.getClient(chantier.client) : null);
			if (idClient == null) {
				this.client = null;
				this.adresses = [];
				this.adresseFacturation = null;
				return;
			}
			this.client = chantier.client;
			const adresses: Adresse[] = this.client.addresses as Adresse[];
			this.adresses = adresses == null ? [] : adresses;
			this.anotherArrayadressesIntervention = adresses == null ? [] : adresses;
			const defaultAdresse = this.client.addresses as Adresse[];
			const adresseFacturation = defaultAdresse.find(a => a.isDefault === true);
			this.form.controls['adresseFacturation'].setValue(adresseFacturation);
			return this.client;
		}
	}

	addAdresse() {
		DialogHelper.openDialog(
			this.dialog,
			AddAdresseComponent,
			DialogHelper.SIZE_LARGE,
			{}
		).subscribe(async adresse => {
			if (adresse !== undefined) {
				if (adresse.isDefault) {
					this.adresses.map(x => x.isDefault = false)
				}
				this.adresses.unshift(adresse);
				this.anotherArrayadressesIntervention = this.adresses;
				this.form.controls['adresseIntervention'].setValue(adresse);
			}
		});
	}

	clearChantier() {
		this.getChantiers('');
		this.getClients('');
	}

	filterclient(e) {
		if (e !== '') {
			this.clients = [];
			this.clients = this.listClients.filter(x => x.name.includes(e) || x.reference.includes(e));
		} else {
			this.clients = this.listClients;
		}
	}

	filterchantier(e) {
		if (e !== '') {
			this.chantiers = [];
			this.chantiers = this.listchantiers.filter(x => x.name.includes(e));
		} else {
			this.chantiers = this.listchantiers;
		}
	}

	async retrunNewClient(client: Client) {
		if (client) {
			await this.getClients('', client);
			// this.listClients.unshift(client);
			// this.form.controls['client'].setValue(client);
			// const adresses: Adresse[] = client.addresses;
			// this.adresses = adresses == null ? [] : adresses;
		}
	}

	// #endregion

	//#region client
	getClient(client) {
		if (client !== undefined) {
			const elem = this.clients.find(x => x.id === client.id);
			if (elem === undefined) { this.clients.unshift(client) };
			return this.clients.find(x => x.id === client.id);
		}
	}
	async LoadchantietClient(facture: Reccurente) {
		// if (facture['maintenanceContract'] != null) { return; }
		this.getChantiers('', facture.document.workshopId);
		this.client = await this.getClient(facture.document.client);
		const defaultAdresse = this.client.addresses as Adresse[];
		const adresseFacturation = defaultAdresse.find(a => a.isDefault === true);
		this.form.controls['adresseFacturation'].setValue(adresseFacturation);
		this.adresses = this.client.addresses as Adresse[];
		this.anotherArrayadressesIntervention = this.client.addresses as Adresse[];
		const adresseIntervention = this.factureReccurente.document.addressIntervention;
		const index = this.getIndexOfAdress(adresseIntervention);
		this.form.controls['adresseIntervention'].setValue(this.adresses[index]);
		return this.client;

	}
	loadClientAdresse(client: Client): Promise<void> {
		return new Promise((resolve, reject) => {
			if (client) {
				const defaultAdresse = client.addresses as Adresse[];
				const adresseFacturation = defaultAdresse.filter(a => a.isDefault === true)[0];
				if (!adresseFacturation) { resolve(); }
				this.form.controls['adresseFacturation'].setValue(adresseFacturation);
				this.adresses = client.addresses as Adresse[];
				this.anotherArrayadressesIntervention = client.addresses as Adresse[];
			}
			if (this.factureReccurente) {
				const adresseIntervention = this.factureReccurente.document.addressIntervention;
				const index = this.getIndexOfAdress(adresseIntervention);
				this.form.controls['adresseIntervention'].setValue(this.adresses[index]);
			}
		})
	}

	updateClient(client: Client, newAdresse: Adresse) {

		this.service.update(ApiUrl.Client, client, client.id).subscribe(res => {
			if (res) {
				this.translate.get('adresse').subscribe(text => {
					// toastr.success('', text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					this.adresses = res.value.addresses;
					this.anotherArrayadressesIntervention = res.value.addresses;
					this.form.patchValue({
						adresseIntervention: newAdresse,
					});
				});
			}
		}, err => {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		});
	}

	//#endregion

	//#region Adresse
	// tslint:disable-next-line: no-shadowed-variable
	getIndexOfAdress(Adresse: Adresse) {
		let index = null;
		this.adresses.forEach((item, i) => {
			if (_.isEqual(item, Adresse)) {
				index = i;
			}
		});
		return index;
	}

	async saveNewAddress(adresses: Adresse[]) {

		const client = this.form.value.client;
		const idClient = client.id;
		if (idClient != null) {
			// tslint:disable-next-line: no-shadowed-variable
			const client = this.clients.find(x => x.id === idClient) as Client;
			const oldAdresse: Adresse[] = client.addresses;
			const index = adresses.length - 1;
			const newAdresse = adresses[index];
			newAdresse.isDefault = false;
			oldAdresse.unshift(newAdresse);
			client.addresses = oldAdresse;
			await this.updateClient(client, newAdresse)
		}

	}
	//#endregion

	//#region set data
	selectDayCheckBox(index) {
		this.daysCheckBox[index].value = !this.daysCheckBox[index].value;
	}
	getDaysSelected() {

		const result = this.daysCheckBox.map(day => {
			if (day.value) {
				return day.type;
			}
		}).filter(x => x != null);

		return result;
	}
	//#endregion
	//#region prestation
	getDataFromArticlesComponet(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.emitter.getDateToSave(res => {
				resolve(res);
			});
		});
	}
	//endregion

	//#region check valid
	checkValiddelaiGarantie(retenueGarantie, delaiGarantie) {
		if (retenueGarantie != 1 && retenueGarantie != null && delaiGarantie == null) {
			this.translate.get('errors').subscribe(text => {
				toastr.warning('Ajouter le délai de garantie', '', {
					positionClass: 'toast-top-center',
					containerId: 'toast-top-center',
				});
			});
			return false

		} else {
			return true
		}
	}
	//#endregion

	//#region update

	getPeriodiciTyOptions() {
		let res;
		if (this.dataDialog !== null) {
			res = {
				daysOfWeek: this.dataDialog.jourSemaine,
				dayOfMonth: this.dataDialog.jour !== undefined ? this.dataDialog.jour : 0,
				recurringType: this.dataDialog.typeRepetition,
				repeatEvery: this.dataDialog.nbrRepetition
			};
		} else if (this.isModif && this.dataDialog === null) {
			res = {
				daysOfWeek: this.getValid(this.factureReccurente.periodicityOptions) ? this.factureReccurente.periodicityOptions.daysOfWeek : [],
				dayOfMonth: this.getValid(this.factureReccurente.periodicityOptions) ? this.factureReccurente.periodicityOptions.dayOfMonth : 0,
				recurringType: this.getValid(this.factureReccurente.periodicityOptions) ? this.factureReccurente.periodicityOptions.recurringType : 0,
				repeatEvery: this.getValid(this.factureReccurente.periodicityOptions) ? this.factureReccurente.periodicityOptions.repeatEvery : 0
			};
		} else {
			res = {
				daysOfWeek: [],
				dayOfMonth: 0,
				recurringType: 0,
				repeatEvery: 0
			};
		}
		return res;
	}

	async createBodyRequest(status) {

		const formValue = this.form.value;
		const reccurent = {
			periodicityOptions: this.getPeriodiciTyOptions(),
			periodicityType: formValue.nbr_repetition,
			endingType: formValue.type_termine,
			occurringCount: formValue.nbr_occurence,
			endingDate: formValue.date_fin !== null && formValue.type_termine === this.periodicityEndingType.SpecificDate
				? AppSettings.formaterDatetime(this.formatDate(formValue.date_fin)) : null,
			startingDate: AppSettings.formaterDatetime(this.formatDate(formValue.date_validation)),
			status: status === this.statutContratEntretien.Brouillon ? this.statutContratEntretien.Brouillon : this.statutContratEntretien.Encours,
			documentType: TypeDocument.Invoice,
			document: await this.generateInvoice(status)
		}
		return reccurent;
	}

	getNameClient(id) {
		const res = this.clients.filter(client => client.id === id)[0];
		return res.firstName + ' ' + res.lastName;
	}

	async generateInvoice(status) {
		const formValue = this.form.value;
		const facture: Invoice = new Invoice();
		facture.reference = '';
		facture.workshopId = formValue.idChantier;
		facture.clientId = formValue.client.id;
		facture.client = formValue.client;
		facture['clientName'] = this.getNameClient(formValue.client.id);
		facture.status = status;
		facture.typeInvoice = TypeFacture.None;
		facture.creationDate = AppSettings.formatDate(new Date());
		facture.dueDate = AppSettings.formatDate(new Date());
		facture.purpose = '';
		facture.note = '';
		facture.paymentCondition = '';
		const dataArticles = await this.getDataFromArticlesComponet();
		const orderDetails: OrderDetails = {
			'globalDiscount': {
				'value': dataArticles.remise,
				'type': dataArticles.typeRemise
			},
			'holdbackDetails': {
				'warrantyPeriod': dataArticles.delaiGarantie,
				'holdback': dataArticles.retenueGarantie,
				'warrantyExpirationDate': new Date(),
			},
			'globalVAT_Value': dataArticles.tvaGlobal,
			'puc': dataArticles.puc,
			'proportion': dataArticles.prorata,
			'productsDetailsType': OrderProductsDetailsType.List,
			'lineItems': dataArticles.prestation,
			'productsPricingDetails': {
				'totalHours': 0,
				'salesPrice': 0
			},
			'totalHT': dataArticles.totalHt,
			'totalTTC': dataArticles.totalTtc
		};
		facture.operationSheetsIds = [];
		facture.orderDetails = orderDetails;
		facture.quoteId = null;
		facture.lotDetails = false;
		facture.OperationSheetMaintenanceId = null;
		facture.ContractId = null;
		facture.addressIntervention = formValue.adresseIntervention;
		return facture;
	}

	getTranslationByKey(key: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get(key).subscribe(translation => {
				resolve(translation)
			});
		});
	}

	close() {
		ManageDataTable.docType = this.docType;
		GlobalInstances.router.navigateByUrl(ManageDataTable.getListRoute());
	}

	save(statut) {
		if (!this.isModif) {
			this.add(statut)
		}
		if (this.isModif) {
			this.update(statut)
		}
	}
	async add(statut) {
		this.prestations = await this.getDataFromArticlesComponet();

		if (this.prestations.prestation.length === 0) {
			const translation = await this.getTranslationByKey('errors');
			toastr.warning(translation.articleRequired, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			this.processing = false;
			return
		}
		if (this.form.value.type_termine === this.periodicityEndingType.Undifined) {
			this.form.value.date_fin = null;
		}
		const result = AppSettings.compareDate(this.form.value.date_validation, this.form.value.date_fin);

		if (result && this.form.value.type_termine === this.periodicityEndingType.SpecificDate) {
			toastr.warning('La date fin de validité doit être supérieure à la date du prochaine échéance'
				, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

			return
		}
		if (this.form.valid && this.prestations.prestation.length !== 0) {
			const createBody = await this.createBodyRequest(statut);


			await this.service.create(ApiUrl.Recurring + ACTION_API.create, createBody).subscribe(async res => {
				if (res.isSuccess) {
					this.translate.get('addFacture').subscribe(text => {
						this.processing = true;
						this.router.navigate(['/factureReccurente/detail', res.value.id]);
						toastr.success(text.msg, text.title, {
							positionClass: 'toast-top-center',
							containerId: 'toast-top-center',
						});
					});
				}
			}, async err => {
				console.log(err);
				this.translate.get('errors').subscribe(text => {
					toastr.warning('text.serveur', '', {
						positionClass: 'toast-top-center',
						containerId: 'toast-top-center',
					});
				}, () => {
					this.processing = false;
				});
			}, () => {
				this.processing = false;
			});

			// if(createBody.endingType)

		} else {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAll, '', {
					positionClass: 'toast-top-center',
					containerId: 'toast-top-center',
				});
			});
		}
	}
	async update(status) {
		debugger
		this.prestations = await this.getDataFromArticlesComponet();
		if (!this.checkValiddelaiGarantie(this.prestations.retenueGarantie, this.prestations.delaiGarantie)) {
			return
		}
		if (this.prestations.prestation.length === 0) {
			const translation = await this.getTranslationByKey('errors');
			toastr.warning(translation.articleRequired, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			this.processing = false;
			return
		}
		if (this.form.value.type_termine === this.periodicityEndingType.Undifined) {
			this.form.value.date_fin = null;
		}
		const result = AppSettings.compareDate(this.form.value.date_validation, this.form.value.date_fin);

		if (result && this.form.value.type_termine === this.periodicityEndingType.SpecificDate) {
			toastr.warning('La date fin de validité doit être supérieure à la date du prochaine échéance'
				, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

			return
		}
		if (!this.form.valid) {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAll, '', {
					positionClass: 'toast-top-center',
					containerId: 'toast-top-center',
				});
			});
			this.processing = false;
			return
		}
		if (this.form.valid && this.prestations.prestation.length !== 0) {
			const createBody = await this.createBodyRequest(status);
			this.service.update(ApiUrl.Recurring, createBody, this.id).subscribe(
				res => {
					if (res) {
						this.processing = true;
						this.translate.get('updateFacture').subscribe(text => {
							toastr.success(text.msg, text.title, {
								positionClass: 'toast-top-center',
								containerId: 'toast-top-center',
							});
							this.router.navigate(['/factureReccurente/detail', res.value.id]);
						});
					}
				},
				err => {
					console.log(err);
					this.translate.get('errors').subscribe(text => {
						toastr.warning(text.serveur, '', {
							positionClass: 'toast-top-center',
							containerId: 'toast-top-center',
						});
					}, () => {
						this.processing = false;
					});
				});
		} else {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAll, '', {
					positionClass: 'toast-top-center',
					containerId: 'toast-top-center',
				});
			});
		}
	}
	//#endregion

	//#region personalise


	selectTypeRecurrent(event) {
		if (event === this.periodicityType.Custom) {
			this.LoadPersonaliseReccurente();
		}
	}

	getValid(res) {
		if (res !== null && res !== undefined) {
			return true
		}
		return false;
	}


	LoadPersonaliseReccurente() {

		const dialogLotConfig = new MatDialogConfig();
		dialogLotConfig.width = '450px';
		dialogLotConfig.height = '500px';
		dialogLotConfig.data = {};
		const res = new ReccurenteModel();
		// tslint:disable-next-line: max-line-length
		res.typeRepetition = this.factureReccurente && this.getValid(this.factureReccurente.periodicityOptions) ? this.factureReccurente.periodicityOptions.recurringType : null;
		// tslint:disable-next-line: max-line-length
		res.nbrRepetition = this.factureReccurente && this.getValid(this.factureReccurente.periodicityOptions) ? this.factureReccurente.periodicityOptions.repeatEvery : null;
		res.jour = this.factureReccurente && this.getValid(this.factureReccurente.periodicityOptions) ? this.factureReccurente.periodicityOptions.dayOfMonth : null;
		res.jourSemaine = this.factureReccurente && this.getValid(this.factureReccurente.periodicityOptions) ? this.factureReccurente.periodicityOptions.daysOfWeek : null;
		dialogLotConfig.data = { listes: res, readOnly: false };
		const dialogRef = this.dialog.open(PersonaliseReccurenteComponent, dialogLotConfig);
		dialogRef.afterClosed().subscribe((data) => {
			this.dataDialog = data;
		});

	}

	formatDate(date) {
		// tslint:disable-next-line: prefer-const
		let d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			// tslint:disable-next-line: prefer-const
			year = d.getFullYear();

		if (month.length < 2) { month = '0' + month; }
		if (day.length < 2) { day = '0' + day; }
		return [year, month, day].join('/');
	}

	//#endregion

}
