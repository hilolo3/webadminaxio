// #region imports
import { Component, OnInit, OnDestroy, AfterViewInit, Inject, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { LocalElements } from 'app/shared/utils/local-elements';
import { DatePipe } from '@angular/common';
import { EditDocumentHelper, DocumentHelper } from 'app/libraries/document-helper';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { AppSettings } from 'app/app-settings/app-settings';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { Adresse } from 'app/Models/Entities/Commun/Adresse';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { StatutDevis } from 'app/Enums/Statut/StatutDevis';
import { Workshop } from 'app/Models/Entities/Documents/Workshop';
import { DialogHelper } from 'app/libraries/dialog';
import { MatDialog, MatInput } from '@angular/material';
import { chantierFromComponent } from 'app/common/chantier-form/from.component';
import { IFormType } from 'app/Enums/IFormType.enum';
import { OrderProductsDetailsType } from 'app/Enums/productsDetailsType.enum';
import { TypeValue } from 'app/Enums/typeValue.Enums';
import * as _ from 'lodash';
import { TypeFacture } from 'app/Enums/TypeFacture.enum';
import { Supplier } from 'app/Models/Entities/Contacts/Supplier';
import { Attachement } from 'app/Models/Entities/Commun/DocumentAttacher';
import { Subscription, of } from 'rxjs';
import { filter, debounceTime, timeout } from 'rxjs/operators';
import { Action } from 'app/Enums/action';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { GlobalInstances } from 'app/app.module';
import { Color } from 'app/Enums/color';
import { AddAdresseComponent } from 'app/common/add-adresse/add-adresse.component';
import { AddClientPopComponent } from 'app/components/add-client-pop/add-client-pop.component';

declare var toastr: any;
declare var jQuery: any;
export declare var swal: any;

export enum TypeDevis {
	Complet = 0,
	Minimalist,
}

// #endregion


@Component({
	selector: 'app-fiche-document',
	templateUrl: './edit-document.component.html',
	styleUrls: [
		'./edit-document.component.scss',
	],
	providers: [DatePipe]
})


export class EditDocumentComponent implements OnInit, OnDestroy, AfterViewInit {

	// #region  Properties

	editHelper = EditDocumentHelper;
	docType = '';
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	action: typeof Action = Action;
	apiUrl: typeof ApiUrl = ApiUrl;
	listClients: Client[] = [];
	listChantiers: Workshop[] = [];
	chantiers: Workshop[] = [];
	fournisseurs: Supplier[] = [];
	listF: Supplier[] = [];
	contrats = [];
	contratsList = [];
	adressesIntervention: Adresse[] = [];
	anotherArrayadressesIntervention: Adresse[] = [];
	selectedStatus = '';
	status = [];
	typeChantier = false;
	typeNone = true;
	typeContrat = false;
	typeClient;
	idChantier: number = null;
	chantiername = '';
	color: typeof Color = Color;
	defaultVal;
	TypeDevis: typeof TypeDevis = TypeDevis;
	typeDevis = TypeDevis.Complet;
	detailLot = false;
	emitter: any = {};
	articles = [];
	retenueGarantie;
	processing = false;
	idDevis = null;
	dataficheInterventionMain;
	idFicheIntervention = null;
	idsBonCommandeFournisseur = [];
	piecesJointes: Attachement[] = [];
	@ViewChild('fromInput', {
		read: MatInput
	}) fromInput: MatInput;
	@ViewChild('froInput', {
		read: MatInput
	}) froInput: MatInput;

	@ViewChild('toInput', {
		read: MatInput
	}) toInput: MatInput;
	subscriptions: Subscription[] = [];
	document = null;
	id;
	factureFIM = false;
	refrenceFicheIM = '';
	clients;
	// #endregion

	// #region Lifecycle

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		public router: Router,
		private route: ActivatedRoute,
		private dialog: MatDialog,
		public formBuilder: FormBuilder,
		public translate: TranslateService,
		public activatedRoute: ActivatedRoute,
	) {
		this.docType = localStorage.getItem(LocalElements.docType);
		EditDocumentHelper.typeAction = EditDocumentHelper.getTypeAction();
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);

		// Loader.show('');
		this.createForm();


		this.subscriptions.push(
			this.router.events.pipe(
				debounceTime(0),
				filter(e => e instanceof NavigationEnd))
				.subscribe(async () => {
					if (
						EditDocumentHelper.isAction(Action.AJOUTER) ||
						EditDocumentHelper.isAction(Action.DUPLIQUER) ||
						EditDocumentHelper.isAction(Action.GENERER)) {
						if (this.docType !== ApiUrl.Depense) {
							EditDocumentHelper.form.controls['reference'].setValue(await this.generateReference());
						}
					}
					if (
						EditDocumentHelper.isAction(Action.MODIFIER) ||
						EditDocumentHelper.isAction(Action.DUPLIQUER) ||
						EditDocumentHelper.isAction(Action.GENERER)) {
						this.id = this.activatedRoute.snapshot.params.id;
						this.processing = true;
						if (EditDocumentHelper.isAction(Action.GENERER) && localStorage.getItem(LocalElements.docTypeGenerer) === 'FactureGroupe') {
							const item = JSON.parse(localStorage.getItem('FactureGroupe'));
							this.idFicheIntervention = item['idsFicheIntervention']
							this.setData(item['facture']);

						} else if (EditDocumentHelper.isAction(Action.GENERER) && localStorage.getItem(LocalElements.docTypeGenerer) === 'DepenseGroupe') {
							const item = JSON.parse(localStorage.getItem('DepenseGroupe'));
							this.idsBonCommandeFournisseur = item['idsBonCommandeFournisseur']
							this.setData(item['depense']);
						} else if (EditDocumentHelper.isAction(Action.GENERER) && localStorage.getItem(LocalElements.docTypeGenerer) === ApiUrl.BonCommandeF) {
							const item = JSON.parse(localStorage.getItem('articlesDepanseSelected'));
							this.setData(item);
						}
						else {
							const doc = await this.getDocument();
							this.setData(doc);

						}
						this.processing = false;

					}


				})
		);

	}


	routour(doc) {
		ManageDataTable.idChantier = this.idChantier;
		ManageDataTable.docType = this.docType;
		ManageDataTable.getDetails(doc);
	}

	async ngOnInit() {
		const res = await this.getParamsFromRoute('idChantier')
		this.idChantier = res === undefined ? null : res as number;
		await this.getClients();
		await this.getChantiers();
		this.isWorkshop();
		await this.getFournisseurs();
		await this.getContrat();

		await this.getDefaultsValues();
	}

	ngAfterViewInit(): void {
		// init helper
	}

	ngOnDestroy(): void {

		localStorage.removeItem(LocalElements.docTypeGenerer);
		if (this.subscriptions) {
			this.subscriptions.forEach(s => s.unsubscribe());
		}
	}

	// #endregion

	// #region Events

	async createForm() {
		EditDocumentHelper.form = this.formBuilder.group({
			reference: ['', [Validators.required]],
			creationDate: [new Date(), [Validators.required]],
			dueDate: ['', [Validators.required]],
			purpose: [''],
			paymentCondition: [''],
			note: [''],
			workshopId: [null],
			client: [''],
			contractId: [''],
			supplier: [''],
			status: [''],
			lotDetails: [false],
			addressIntervention: [null],
		});

		!this.isDocSupplier() ? EditDocumentHelper.form.get('client').setValidators([Validators.required]) :
			EditDocumentHelper.form.get('supplier').setValidators([Validators.required]);
		if (this.isAdresseIntervention()) { EditDocumentHelper.form.get('addressIntervention').setValidators([Validators.required]); }
	}

	filterclient(e) {
		if (e !== '') {
			this.listClients = [];
			this.listClients = this.clients.filter(x => x.name.includes(e.toUpperCase()) || x.reference.includes(e.toUpperCase()));
		} else {
			this.listClients = this.clients;
		}

	}

	filterAdressInterv(e) {
		if (e !== '') {
			this.adressesIntervention = [];
			this.adressesIntervention = this.anotherArrayadressesIntervention.filter(x => x.designation.includes(e.toUpperCase()));
		} else {
			this.adressesIntervention = this.anotherArrayadressesIntervention;
		}
	}

	filterchantier(e) {
		if (e !== '') {
			this.listChantiers = [];
			this.listChantiers = this.chantiers.filter(x => x.name.includes(e.toUpperCase()));
		} else {
			this.listChantiers = this.chantiers;
		}
	}

	filtercontrat(e) {
		if (e !== '') {
			this.contrats = [];
			this.contrats = this.contratsList.filter(x => x.name.includes(e.toUpperCase()));
		} else {
			this.contrats = this.contratsList;
		}
	}

	filtersuppluer(e) {
		if (e !== '') {
			this.listF = [];
			this.listF = this.fournisseurs.filter(x => x.firstName.includes(e.toUpperCase()) || x.reference.includes(e.toUpperCase()));
		} else {
			this.listF = this.fournisseurs;
		}
	}

	addClient() {
		DialogHelper.openDialog(
			this.dialog,
			AddClientPopComponent,
			DialogHelper.SIZE_LARGE,
			{ docType: ApiUrl.Client }
		).subscribe(async client => {
			if (client !== undefined) {
				await this.getClients();
				this.listClients.unshift(client);
				EditDocumentHelper.form.controls['client'].setValue(client);
			}
		});
	}

	addSupplier() {
		DialogHelper.openDialog(
			this.dialog,
			AddClientPopComponent,
			DialogHelper.SIZE_LARGE,
			{ docType: ApiUrl.Fournisseur }
		).subscribe(async fournisseur => {
			if (fournisseur !== undefined) {
				await this.getFournisseurs();
				this.listF.unshift(fournisseur);
				EditDocumentHelper.form.controls['supplier'].setValue(fournisseur);
			}
		});
	}

	addAdresse() {
		DialogHelper.openDialog(
			this.dialog,
			AddAdresseComponent,
			DialogHelper.SIZE_LARGE,
			{}
		).subscribe(async adresse => {
			if (adresse !== undefined) {
				if (adresse.isDefault) {
					this.adressesIntervention.map(x => x.isDefault = false)
				}
				this.adressesIntervention.unshift(adresse);
				this.anotherArrayadressesIntervention = this.adressesIntervention;
				EditDocumentHelper.form.controls['addressIntervention'].setValue(adresse);
			}
		});
	}

	setData(doc) {
		setTimeout(async () => {
			this.processing = true;
			try {
				EditDocumentHelper.form.patchValue({
					purpose: doc.purpose,
					paymentCondition: doc.paymentCondition,
					note: doc.note,
					//	workshopId: doc.workshop && doc.workshop !== null ? doc.workshop.id : null,
					contractId: doc.contract && doc.contract !== null ? doc.contract.id : null,
					supplier: doc.supplier,
					status: doc.status,
					//	client: EditDocumentHelper.isAction(Action.DUPLIQUER) ? null : doc.client,
					lotDetails: doc.lotDetails,
					addressIntervention: doc.addressIntervention
				});
				if (!this.isDocSupplier()) {
					this.typeDevis = doc.orderDetails.productsDetailsType;
				}

				if (this.isModif()) {
					this.selectedStatus = doc.status;
					this.CheckStatus();
				}

				if (EditDocumentHelper.isAction(Action.MODIFIER)) {
					EditDocumentHelper.form.controls.reference.setValue(doc.reference);
					EditDocumentHelper.form.controls.creationDate.setValue(new Date(doc.creationDate));
					EditDocumentHelper.form.controls.dueDate.setValue(new Date(doc.dueDate));
				}

				if (EditDocumentHelper.isAction(Action.GENERER) && localStorage.getItem(LocalElements.docTypeGenerer) === ApiUrl.Devis) {
					this.idDevis = doc.id;
					this.typeDevis = TypeDevis.Complet;
				}

				if (EditDocumentHelper.isAction(Action.GENERER) && localStorage.getItem(LocalElements.docTypeGenerer) === ApiUrl.FicheIntervention) {
					this.idFicheIntervention = [doc.id];
					this.typeDevis = TypeDevis.Complet;
				}

				if (EditDocumentHelper.isAction(Action.GENERER) && localStorage.getItem(LocalElements.docType) === ApiUrl.Depense &&
					localStorage.getItem(LocalElements.docTypeGenerer) === ApiUrl.BonCommandeF) {
					this.idsBonCommandeFournisseur.push(doc.id)
				}

				if (EditDocumentHelper.isAction(Action.GENERER) && localStorage.getItem(LocalElements.docType) === ApiUrl.Invoice &&
					localStorage.getItem(LocalElements.docTypeGenerer) === ApiUrl.FicheInterventionMaintenance) {
					this.typeDevis = TypeDevis.Complet;
					this.factureFIM = true;
					this.refrenceFicheIM = doc.reference;
				}


				if (EditDocumentHelper.isAction(Action.GENERER) && localStorage.getItem(LocalElements.docType) === ApiUrl.Invoice &&
					localStorage.getItem(LocalElements.docTypeGenerer) === ApiUrl.MaintenanceContrat) {
					this.typeContrat = true;
					this.typeNone = false;
					EditDocumentHelper.form.controls.contractId.setValue(doc.id);
				}


				if (doc.workshop && doc.workshop !== null) {
					this.typeChantier = true;
					this.typeNone = false;
					await this.loadClientofChantier(doc.workshop.id);
				}

				if (doc.operationSheetMaintenance && doc.operationSheetMaintenance !== null) {
					this.factureFIM = true;
					this.refrenceFicheIM = doc.operationSheetMaintenance.reference;
				}

				if (doc.contract && doc.contract !== null) {
					this.typeContrat = true;
					this.typeNone = false;
				}

				if (this.idChantier !== null && this.listChantiers !== undefined) {
					const res = this.listChantiers.find(x => x.id === doc.workshop.id);
					this.chantiername = res.name;
				}
				// if (this.idChantier !== null && this.listChantiers !== undefined && !EditDocumentHelper.isAction(Action.DUPLIQUER)) {
				// 	const res = this.listChantiers.find(x => x.id === doc.workshop.id);
				// 	this.chantiername = res.name;
				// }
				if (this.isAdresseIntervention() && doc.client !== null) {
					this.adressesIntervention = doc.client.addresses == null ? [] : doc.client.addresses;
					this.anotherArrayadressesIntervention = this.adressesIntervention;
					const index = this.getIndexOfAdress(doc.addressIntervention);
					EditDocumentHelper.form.controls.addressIntervention.setValue(this.adressesIntervention[index]);
				}
				if (!EditDocumentHelper.isAction(Action.DUPLIQUER)) {

					EditDocumentHelper.form.controls.workshopId.setValue(doc.workshop && doc.workshop !== null ? doc.workshop.id : null);
					if (!this.isDocSupplier()) {
						await this.isClientinList(doc.client);
						EditDocumentHelper.form.controls.client.setValue(this.listClients.find(x => x.id === doc.client.id));
					}
				}

				if (this.isDocSupplier()) {
					await this.isFournisseurinList(doc.supplier);
					EditDocumentHelper.form.controls.supplier.setValue(this.fournisseurs.find(x => x.id === doc.supplier.id));
				}
				if (EditDocumentHelper.isAction(Action.DUPLIQUER)) {
					EditDocumentHelper.form.controls.workshopId.setValue(null);
					EditDocumentHelper.form.controls.client.setValue(null);

				}
				if (this.idChantier != null && EditDocumentHelper.isAction(Action.DUPLIQUER)) {
					EditDocumentHelper.form.controls.workshopId.setValue(doc.workshop && doc.workshop !== null ? doc.workshop.id : null);

				}

				this.detailLot = doc.lotDetails === true ? true : false;

				this.articles = doc.orderDetails.lineItems;


				if (this.docType === ApiUrl.Depense) {
					this.piecesJointes = doc.attachments === undefined || doc.attachments === null ? [] : doc.attachments;
				}

				// if (this.isHold()) {
				// 	this.participationPuc = (doc['orderDetails']['puc'] > 0);
				// 	this.partProrata = (doc['orderDetails']['proportion'] > 0);
				// 	this.retenuGaranti = (doc['orderDetails']['holdbackDetails'] !== null && doc['orderDetails']['holdbackDetails']['holdback'] > 0);
				// }

				this.processing = false;
				// Loader.hide();
			} catch (e) {
				this.processing = false;
				console.log({ e });
				// Loader.hide();
			}
		}, 0);
	}

	getColor(status) { return this.color[status] };


	// #endregion

	// #region action

	addChantier() {
		DialogHelper.openDialog(
			this.dialog,
			chantierFromComponent,
			DialogHelper.SIZE_MEDIUM,
			{ data: { defaultData: null, type: IFormType.add } }
		).subscribe(async response => {
			EditDocumentHelper.form.controls.workshopId.setValue(response.id.toString());
			this.listChantiers = await this.getChantiers();
			this.loadClientofChantier(response.id);
		});

	}

	createMinimalistBodyRequest(statut) {
		return new Promise(async (reslove, reject) => {
			let values = EditDocumentHelper.form.value;
			values = AppSettings.ConvertEmptyValueToNull(values);
			const dataArticles = await this.getDataFromArticlesMinimaliste();

			var object = {};

			if (EditDocumentHelper.isAction(Action.MODIFIER)) {
				object = this.document;
			}

			const orderDetails = {
				'globalDiscount': {
					'value': 0,
					'type': TypeValue.Amount
				},
				'holdbackDetails': {
					'warrantyPeriod': dataArticles.delaiGarantie,
					'holdback': dataArticles.retenueGarantie,
					'warrantyExpirationDate': new Date(),
				},
				'globalVAT_Value': dataArticles.data.tva,
				'puc': dataArticles.data.puc,
				'proportion': dataArticles.data.prorata,
				'productsDetailsType': OrderProductsDetailsType.File,
				'lineItems': [],
				'productsPricingDetails': {
					'totalHours': +dataArticles.data.nomber_heure,
					'hourlyCost': dataArticles.data.cout_vente,
					'materialCost': dataArticles.data.cout_materiel,
					'achatsMateriels': dataArticles.data.achat_materiel
				},
				'productsDetails_File': {
					'fileId': dataArticles.data.devisExel.content,
					'fileName': dataArticles.data.devisExel.fileName,
					'fileType': dataArticles.data.devisExel.fileType,
				},
				'totalHT': dataArticles.totalHt,
				'totalTTC': dataArticles.totalTtc
			};
			object['creationDate'] = AppSettings.formaterDatetime(values.creationDate);
			object['dueDate'] = AppSettings.formaterDatetime(values.dueDate);
			object['reference'] = values.reference;
			object['orderDetails'] = orderDetails;
			object['note'] = values.note;
			object['paymentCondition'] = values.paymentCondition;
			object['purpose'] = values.purpose;
			object['workshopId'] = values.workshopId;
			object['addressIntervention'] = values.addressIntervention;
			object['clientId'] = values.client.id;
			object['client'] = values.client;


			if (!EditDocumentHelper.isAction(Action.MODIFIER)) {
				if (statut) {
					object['status'] = statut;
					object['reference'] = values.reference + '-' + AppSettings.generateStampHM() + '-Brouillon';

				} else {
					object['status'] = StatutDevis.EnAttente
				}
			}
			else {
				if (this.selectedStatus === 'draft' && !statut) {
					object['status'] = StatutDevis.EnAttente;
					object['reference'] = await this.generateReference();
				}
				if (this.selectedStatus === 'draft' && statut) {
					object['status'] = statut;

				}
			}


			await this.addAdresseToClient(values.addressIntervention);

			reslove(object);
		});
	}

	async createCompletBodyRequest(statut) {
		return new Promise(async (reslove, reject) => {
			let values = EditDocumentHelper.form.value;

			var object = {};

			if (EditDocumentHelper.isAction(Action.MODIFIER)) {
				object = this.document;
			}

			values = AppSettings.ConvertEmptyValueToNull(values);

			const dataArticles = await this.getDataFromArticlesComponet();
			if (dataArticles.prestation === []) {
				reslove(null);
			}
			if (!this.checkValiddelaiGarantie(dataArticles.retenueGarantie, dataArticles.delaiGarantie)) {
				return
			}

			const orderDetails = !this.isDocSupplier() ? {
				'globalDiscount': {
					'value': dataArticles.remise,
					'type': dataArticles.typeRemise
				},
				'holdbackDetails': {
					'warrantyPeriod': dataArticles.delaiGarantie,
					'holdback': dataArticles.retenueGarantie,
					'warrantyExpirationDate': new Date(),
				},
				'globalVAT_Value': dataArticles.tvaGlobal,
				'puc': dataArticles.puc,
				'proportion': dataArticles.prorata,
				'productsDetailsType': OrderProductsDetailsType.List,
				'lineItems': dataArticles.prestation,
				'productsPricingDetails': {
					'totalHours': 0,
					'salesPrice': 0
				},
				'totalHT': dataArticles.totalHt,
				'totalTTC': dataArticles.totalTtc
			} : {
					'lineItems': dataArticles.prestation,
					'totalHT': dataArticles.totalHt,
					'totalTTC': dataArticles.totalTtc,
				};
			object['reference'] = values.reference;
			object['creationDate'] = AppSettings.formaterDatetime(values.creationDate);
			object['dueDate'] = AppSettings.formaterDatetime(values.dueDate);
			object['orderDetails'] = orderDetails;
			object['note'] = values.note;
			object['paymentCondition'] = values.paymentCondition;
			object['purpose'] = values.purpose;
			object['workshopId'] = this.typeChantier ? values.workshopId : null;
			object['lotDetails'] = this.detailLot;

			if (!EditDocumentHelper.isAction(Action.MODIFIER)) {
				if (statut) {
					object['status'] = statut;
					object['reference'] = values.reference + '-' + AppSettings.generateStampHM() + '-Brouillon';

				} else {
					object['status'] = StatutDevis.EnAttente
				}
			}
			else {
				if (this.selectedStatus === 'draft' && !statut) {
					object['status'] = StatutDevis.EnAttente;
					object['reference'] = await this.generateReference();
				}
				if (this.selectedStatus === 'draft' && statut) {
					object['status'] = statut;

				}
				if (this.selectedStatus !== 'draft' && !statut) {
					object['status'] = this.selectedStatus;

				}
			}

			if (this.isDocSupplier()) {
				object['supplierId'] = values.supplier.id;
				object['supplier'] = values.supplier;
			} else {
				object['clientId'] = values.client.id;
				object['client'] = values.client;
			}

			if (this.docType === ApiUrl.Depense) {
				object['expenseCategoryId'] = 11;
				object['supplierOrdersId'] = this.idsBonCommandeFournisseur;
				object['attachments'] = this.piecesJointes;
			}

			if (this.isAdresseIntervention()) { object['addressIntervention'] = values.addressIntervention; }

			if (this.docType === ApiUrl.Invoice) {
				object['typeInvoice'] = TypeFacture.None;
				object['ContractId'] = this.typeContrat ? values.contractId : null;

				object['operationSheetsIds'] = this.idFicheIntervention !== null ? this.idFicheIntervention : [];
				object['quoteId'] = this.idDevis;
				object['OperationSheetMaintenanceId'] = this.dataficheInterventionMain !== undefined ? this.dataficheInterventionMain.id : null;
			}

			await this.addAdresseToClient(values.addressIntervention);
			reslove(object)
		});

	}

	async submit(statut?) {

		this.processing = true;
		if (await this.checkIfValidation() === false) {
			const translation = await this.getTranslationByKey('errors');
			toastr.warning(translation.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			this.processing = false;
			return;
		}
		let bodyRequest = null;
		if (this.typeDevis === this.TypeDevis.Complet) {
			bodyRequest = await this.createCompletBodyRequest(statut);
			if (bodyRequest.orderDetails.lineItems.length === 0) {
				const translation = await this.getTranslationByKey('errors');
				toastr.warning(translation.articleRequired, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				this.processing = false;
				return
			}
		} else if (this.typeDevis === this.TypeDevis.Minimalist) {
			bodyRequest = await this.createMinimalistBodyRequest(statut);
		}
		if (new Date(bodyRequest['creationDate'].split('T')[0]) > new Date(bodyRequest['dueDate'].split('T')[0])) {
			this.processing = false;
			toastr.warning('Date d echenace doit etre superieur ou égale à la date de création ',
				'', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			return;
		}

		await this.insertDocument(bodyRequest);
	}

	async insertDocument(bodyRequest) {

		if (EditDocumentHelper.isAction(Action.AJOUTER) ||
			EditDocumentHelper.isAction(Action.DUPLIQUER) ||
			EditDocumentHelper.isAction(Action.GENERER)) {
			await this.create(bodyRequest);
		}

		if (EditDocumentHelper.isAction(Action.MODIFIER)) {
			await this.update(bodyRequest);
		}
	}

	//#endregion

	// #region operation

	async chargeDates() {

		if (
			EditDocumentHelper.isAction(Action.AJOUTER) ||
			EditDocumentHelper.isAction(Action.DUPLIQUER) ||
			EditDocumentHelper.isAction(Action.GENERER)) {
			const defaultsValues = this.defaultVal.find(x => x.documentType === this.getTypeNumer());
			if (defaultsValues) {
				const dateNow = new Date();
				dateNow.setDate(dateNow.getUTCDate() + parseInt(defaultsValues['validity'], 0))
				EditDocumentHelper.form.controls['creationDate'].setValue(new Date());
				EditDocumentHelper.form.controls['dueDate'].setValue(dateNow);
			}
		}
	}



	getAdressFacturation() {
		const client = EditDocumentHelper.form.controls.client.value;
		if (client) {
			const adresseFacturation: Adresse[] = client.addresses as Adresse[];
			const result: Adresse = (adresseFacturation && adresseFacturation.length > 0 ?
				adresseFacturation.find(A => A.isDefault === true) : '') as Adresse;
			return result ? result.designation : '';
		}
		return '';
	}

	async loadClientofChantier(idChantier) {
		try {
			if (this.listChantiers.length === 0) { await this.getChantiers() }
			if (this.listChantiers.length !== 0 && idChantier != null) {
				const chantierInfos = this.listChantiers.find(x => x.id === idChantier);
				const clientInfo = chantierInfos.client;
				if (clientInfo !== undefined && !this.isDocSupplier()) {
					this.isClientinList(clientInfo);
					const client = this.listClients.find(x => x.id === clientInfo.id);
					EditDocumentHelper.form.controls['client'].setValue(client);
					if (clientInfo.note != null && clientInfo.note !== undefined) {
						EditDocumentHelper.form.controls['note'].setValue(clientInfo.note);
					}
					if (clientInfo.paymentCondition != null && clientInfo.paymentCondition !== undefined) {
						EditDocumentHelper.form.controls['paymentCondition'].setValue(clientInfo.paymentCondition);
					}
					this.getAdressIntervention();
				}

			}
		} catch (e) {
			this.processing = false;
			console.log({ e });
			// Loader.hide();
		}
	}


	async isClientinList(client) {
		if (this.listClients.length === 0) { await this.getClients(); }
		const dataClient = this.listClients.find(x => x.id === client.id);
		if (dataClient === undefined) {
			this.listClients.unshift(client);
		}
	}

	async isFournisseurinList(supplier) {
		if (this.listF.length === 0) {
			await this.getFournisseurs()
		}
		const dataClient = this.listF.find(x => x.id === supplier.id);
		if (dataClient === undefined) {
			this.listF.unshift(supplier);
		}
	}

	isModif() { return EditDocumentHelper.isAction(Action.MODIFIER); }

	isChantierinList(id) {
		const dataClient = this.listChantiers.find(x => x.id === id);
		return dataClient === undefined ? false : true;
	}

	getAdressIntervention() {
		try {
			const client = EditDocumentHelper.form.controls.client.value;
			if (client) {
				if (client.note != null && client.note != undefined) {
					EditDocumentHelper.form.controls['note'].setValue(client.note);

				}
				if (client.paymentCondition != null && client.paymentCondition != undefined) {
					EditDocumentHelper.form.controls['paymentCondition'].setValue(client.paymentCondition);

				}
				const adresses: Adresse[] = client.addresses;
				this.adressesIntervention = adresses == null ? [] : adresses;
				this.anotherArrayadressesIntervention = this.adressesIntervention;
				const result: Adresse = (adresses && adresses.length > 0 ?
					adresses.find(A => A.isDefault === true) : '') as Adresse;
				EditDocumentHelper.form.controls.addressIntervention.setValue(result);
			}
		} catch (e) {
			this.processing = false;
			console.log({ e });
			// Loader.hide();
		}
	}

	getTypeNumer() {
		if (this.docType === ApiUrl.Devis) { return this.typeNumerotation.devis };
		if (this.docType === ApiUrl.Invoice) { return this.typeNumerotation.facture };
		if (this.docType === ApiUrl.avoir) { return this.typeNumerotation.avoir };
		if (this.docType === ApiUrl.BonCommandeF) { return this.typeNumerotation.boncommande_fournisseur };
		if (this.docType === ApiUrl.Depense) { return this.typeNumerotation.depense };
	}

	async isWorkshop() {
		if (this.idChantier != null) {
			this.typeChantier = true;
			EditDocumentHelper.form.controls['workshopId'].setValue(this.idChantier.toString());

			this.loadClientofChantier(this.idChantier);
			const chantier = this.listChantiers.find(x => x.id === this.idChantier.toString());
			this.chantiername = chantier.name;
		}
	}

	getDataFromArticlesComponet(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.emitter.getDateToSave((res) => {
				resolve(res);
			})
		})
	}

	getDataFromArticlesMinimaliste(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.emitter.getData((res) => {
				resolve(res);
			})
		})
	}

	getTranslationByKey(key: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get(key).subscribe(translation => {
				resolve(translation)
			});
		});
	}

	loadContratClient(idContrat) {
		const client = this.contrats.find(c => c.id === idContrat).client;
		EditDocumentHelper.form.controls['client'].setValue(client);
		this.getAdressIntervention();

	}

	loadInfoFournisseur(supplierId) {
		if (supplierId) {
			const fournisseur = this.listF.find(x => x.id === supplierId);
			this.articles = [];
			if (fournisseur.note != null && fournisseur.note != undefined) {
				EditDocumentHelper.form.controls['note'].setValue(fournisseur.note);

			}
			if (fournisseur.paymentCondition != null && fournisseur.paymentCondition != undefined) {
				EditDocumentHelper.form.controls['paymentCondition'].setValue(fournisseur.paymentCondition);

			}
		}
	}

	startUpload(event: FileList): void {
		this.processing = true;
		const file = event.item(0)
		const reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => {
			const pieceJoin = new Attachement()
			pieceJoin.fileId = ''
			pieceJoin.fileType = file.name.substring(file.name.lastIndexOf('.') + 1)
			pieceJoin.fileName = file.name
			pieceJoin.content = reader.result.toString();
			this.piecesJointes.push(pieceJoin);
		}
		this.processing = false;
	}

	downloadPieceJointe(index: number) {
		const piece = this.piecesJointes[index];
		if (piece.fileId !== '') {
			this.service.getById(ApiUrl.File, piece.fileId).subscribe(value => {
				AppSettings.downloadBase64(
					value.value,
					piece.fileName,
					value.value.substring('data:'.length, value.value.indexOf(';base64')),
					piece.fileType
				);
			});
		} else {
			AppSettings.downloadBase64(
				piece.content,
				piece.fileName,
				piece.content.substring('data:'.length, piece.content.indexOf(';base64')),
				piece.fileType
			);
		}

	}


	removeFile(index: number) {
		this.piecesJointes.splice(index, 1);
	}

	close() {
		ManageDataTable.idChantier = this.idChantier;
		ManageDataTable.docType = this.docType;
		GlobalInstances.router.navigateByUrl(ManageDataTable.getListRoute());
	}

	// #endregion

	// #region check
	isHold() {
		return !(EditDocumentHelper.isAction(Action.GENERER) &&
			localStorage.getItem(LocalElements.docTypeGenerer) === ApiUrl.FicheIntervention)
			&& !(EditDocumentHelper.isAction(Action.GENERER) &&
				localStorage.getItem(LocalElements.docTypeGenerer) === ApiUrl.FicheInterventionMaintenance)
	}

	isGenererDepenseDepuisBCF() {
		return this.docType === ApiUrl.Depense &&
			localStorage.getItem(LocalElements.docTypeGenerer) === ApiUrl.BonCommandeF
	}

	isDocSupplier() { return this.docType === ApiUrl.Depense || this.docType === ApiUrl.BonCommandeF }

	isAdresseIntervention() { return this.docType !== ApiUrl.avoir && !this.isDocSupplier(); }

	isdetailLot() { return this.docType === ApiUrl.Invoice || this.docType === ApiUrl.Devis; }

	CheckUniqueReference(control: FormControl): Promise<any | null> {
		if (control.value !== '' && this.docType) {
			const promise = new Promise((resolve, reject) => {
				this.service
					.getById(this.docType + ApiUrl.CheckReference, control.value)
					.subscribe(res => {
						if (res === false) {
							resolve({ CheckUniqueReference: true });
						} else {
							resolve(null);
						}
					});
			});
			return promise;
		}
	}

	changedispalyToClient(value) {
		this.typeClient = value;
		if (+value === 1) {
			this.typeNone = true;
			this.typeChantier = false;
			this.typeContrat = false;
		} else {
			this.typeNone = false;
		}
		if (+value === 2) {
			this.typeContrat = true;
			this.typeNone = false;
			this.typeChantier = false;
			EditDocumentHelper.form.get('contractId').setValidators([Validators.required]);
		} else {
			this.typeContrat = false;
			EditDocumentHelper.form.get('contractId').clearValidators();
		}
		if (+value === 3) {
			this.typeChantier = true;
			this.typeNone = false;
			this.typeContrat = false;
			EditDocumentHelper.form.get('workshopId').setValidators([Validators.required]);
		} else {
			this.typeChantier = false;
			EditDocumentHelper.form.get('workshopId').clearValidators();
		}
	}


	checkTypePres() { return this.docType === this.apiUrl.Devis && !this.editHelper.isAction(this.action.MODIFIER) }


	CheckStatus() {
		if (this.docType === ApiUrl.Devis && this.selectedStatus === StatutDevis.EnAttente) {
			this.status = [StatutDevis.EnAttente, StatutDevis.NonAcceptee, StatutDevis.Acceptee, StatutDevis.Annulee];
		}
		if (this.docType === ApiUrl.Devis && this.selectedStatus === StatutDevis.Acceptee) {
			this.status = [StatutDevis.EnAttente, StatutDevis.NonAcceptee, StatutDevis.Acceptee, StatutDevis.Annulee];
		}
		if (this.docType === ApiUrl.Devis && this.selectedStatus === StatutDevis.NonAcceptee) {
			this.status = [StatutDevis.NonAcceptee, StatutDevis.EnAttente, StatutDevis.Annulee];
		}
		if (this.docType === ApiUrl.Devis && this.selectedStatus === StatutDevis.Annulee) {
			this.status = [StatutDevis.Annulee, StatutDevis.EnAttente];
		}
	}

	onStatusChanged(e: any): void {
		this.selectedStatus = e;
	}

	get f() { return EditDocumentHelper.form.controls; }

	toast(msg) {
		return toastr.success(this.translate.instant('toast.' + msg), '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
	}

	getParamsFromRoute(paramName: string): Promise<string | number> {
		return new Promise((resolve, reject) => {
			this.route.params.subscribe(params => resolve(params[paramName]))
		});
	}

	click(e) {
		this.detailLot = e;
	}


	checkValiddelaiGarantie(retenueGarantie, delaiGarantie) {
		if (retenueGarantie !== 1 && retenueGarantie != null && delaiGarantie == null) {
			this.translate.get('errors').subscribe(text => {
				toastr.warning('Ajouter le délai de garantie', '', {
					positionClass: 'toast-top-center',
					containerId: 'toast-top-center',
				});
			});
			return false
		} else {
			return true
		}
	}

	async checkIfValidation(): Promise<boolean> {
		const condition = this.typeDevis === this.TypeDevis.Minimalist ? (await this.getDataFromArticlesMinimaliste()) !== false : true;
		return (EditDocumentHelper.form.valid && condition);
	}

	getIndexOfAdress(adresse: Adresse) {
		let index = null;
		this.adressesIntervention.forEach((item, i) => {
			if (_.isEqual(item, adresse)) {
				index = i;
			}
		});
		return index;
	}
	//#endregion

	// #region services

	async getClients(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.getAll(ApiUrl.Client).subscribe((res) => {
				resolve(res.value);
				this.clients = res.value;
				this.listClients = res.value;
			});
		});
	}


	async getFournisseurs(): Promise<any> {
		if (this.listF.length === 0) {
			return new Promise((resolve, reject) => {
				this.service.getAll(ApiUrl.Fournisseur).subscribe((res) => {
					resolve(res.value);
					this.fournisseurs = res.value;
					this.listF = res.value;

				});
			});
		}
	}

	async getChantiers(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.getAll(ApiUrl.Chantier).subscribe((res) => {
				resolve(res.value);
				this.listChantiers = res.value;
				this.chantiers = res.value;
			});
		});
	}


	getDefaultsValues(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.getAll(ApiUrl.configDocument).subscribe(res => {
				this.defaultVal = JSON.parse(res);
				if (this.defaultVal != null) {
					const defaultsValues = this.defaultVal.find(x => x.documentType === this.getTypeNumer());
					if (defaultsValues && !EditDocumentHelper.isAction(Action.MODIFIER) && !EditDocumentHelper.isAction(Action.DUPLIQUER) && !EditDocumentHelper.isAction(Action.GENERER)) {
						EditDocumentHelper.form.controls['note'].setValue(defaultsValues['note']);
						// EditDocumentHelper.form.controls['dueDate'].setValue(defaultsValues['validity']);
						EditDocumentHelper.form.controls['paymentCondition'].setValue(defaultsValues['paymentTerms']);
						this.detailLot = defaultsValues['LotDetails'] as boolean;
						this.retenueGarantie = defaultsValues['Holdback'];
						this.chargeDates();

					}
				}
			});
		});
	}

	getChantierById(idChantier): Promise<Workshop> {
		return new Promise((resolve, reject) => {
			this.service.getById(ApiUrl.Chantier, idChantier)
				.subscribe(
					res => resolve(res.value),
					error => reject(error)
				)
		});
	}

	generateReference(): Promise<string> {
		if (this.docType) {
			return new Promise((resolve, reject) => {
				this.service.getAll(ApiUrl.configReference + this.getTypeNumer() + '/reference')
					.subscribe(response => {
						resolve(response);
					}, (error) => reject(error));
			});
		}
	}

	addAdresseToClient(nouveauAdresses: any): Promise<void> {
		return new Promise((resolve, reject) => {
			const adresse = this.adressesIntervention.filter(a => _.isEqual(a, nouveauAdresses));
			if (adresse.length !== 0) {
				const client = EditDocumentHelper.form.value.client;
				this.service.update(ApiUrl.Client, client, client.id).subscribe(
					res => { resolve() }, err => reject());
			} else {
				resolve();
			}
		});
	}

	getContrat() {
		this.service.getAll(ApiUrl.MaintenanceContrat)
			.subscribe(async res => {
				this.contrats = res.value;
				this.contratsList = res.value;
			}, err => {
				this.translate.get('errors').subscribe(text => {
					toastr.warning(text.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
			}, () => {
				this.processing = false;
			});
	}


	getDocument(): Promise<any> {
		return new Promise((resolve, reject) => {
			if (this.id && this.id !== '') {
				const docType = EditDocumentHelper.isAction(Action.GENERER) && this.id !== '' ? localStorage.getItem(LocalElements.docTypeGenerer) : this.docType;
				this.service.getById(docType, this.id).subscribe(res => {
					this.document = res.value;
					resolve(res.value);
				});
			}
		});
	}


	create(bodyRequest): Promise<any> {

		return new Promise((resolve, reject) => {
			this.service.create(this.docType + ACTION_API.create, bodyRequest).subscribe(async res => {
				this.toast('add-sucsess');
				this.routour(res.value);
				this.processing = false;
				resolve();
			}, async err => {
				console.log(err);
				if (err.error && err.error.messageCode === 103) {
					const reference = EditDocumentHelper.form.value.reference;
					EditDocumentHelper.form.controls['reference'].setValue(await this.generateReference())
					const message = "Il existe un document avec cette reference :  " + reference + "   Vous voulez créer cet document par la nouevelle reference :"
						+ EditDocumentHelper.form.value.reference;
					swal({
						title: '',
						text: message,
						icon: 'warning',
						buttons: {
							cancel: {
								text: 'Annuler',
								value: null,
								visible: true,
								className: '',
								closeModal: true
							},
							confirm: {
								text: 'Valider',
								value: true,
								visible: true,
								className: '',
								closeModal: true
							}
						}
					}).then(async isConfirm => {
						if (isConfirm) {
							bodyRequest['reference'] = EditDocumentHelper.form.value.reference;
							this.create(bodyRequest);
						} else {
						}
					});

				} else {
					const translation = await this.getTranslationByKey('errors');
					toastr.warning(translation.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					this.processing = false;
					resolve();
				}
			});
		});
	}

	update(bodyRequest): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.update(this.docType, bodyRequest, bodyRequest.id).subscribe(async res => {
				this.toast('update-sucsess');
				this.routour(res.value);
				this.processing = false;
				resolve();
			}, async err => {
				console.log(err);
				const translation = await this.getTranslationByKey('errors');
				toastr.warning(translation.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				this.processing = false;
				resolve();
			});
		})
	}



	// #endregion

}
