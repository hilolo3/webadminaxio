import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { LocalElements } from '../utils/local-elements';

@Component({
	selector: 'app-navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent implements OnInit {
	user: any;
	sidebarToggled = true;
	constructor(private router: Router, private translate: TranslateService) {

	}

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.user = JSON.parse(localStorage.getItem('PDB_USER'));
	}

	logout() {
		localStorage.removeItem(LocalElements.TOKEN);
		localStorage.removeItem(LocalElements.PDB_USER);
		this.router.navigate(['/login'])
	}

	getProfile() {

		this.router.navigate([`utilisateurs/detail/${this.user.id}`])
	}
	setUserIdInlocalStorage(id: number) {
		localStorage.setItem('SBP_changePassword_UserId', id.toString());
	}

	sidebarToogle() {
		this.sidebarToggled = !this.sidebarToggled;
		const navItems = document.querySelectorAll('.sidebarDisplay');

		[].forEach.call(navItems, (e) => {
			e.style.display = !this.sidebarToggled ? 'none' : 'block';
		});

	}
}
