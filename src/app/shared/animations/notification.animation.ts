import { trigger, state, transition, style, animate } from '@angular/animations';

export const notificationAnimations = [
	trigger('dismiss', [
		state('visible', style({})),
		transition('visible => void', [
			animate('0.3s',
				style({
					opacity: '0',
					transform: 'translateX(-100%)'
				})
			),
		]),
	])
];
