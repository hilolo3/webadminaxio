import { SendMailParams } from 'app/Models/Entities/Commun/SendMailParams';

export interface IGenericRepository<T> {
	delete(url: string, idDoc: string);
	deleteAll(url: string,);
	getAllPagination(url: string, filterOption: any);
	getById<T>(url: string, id: any);
	update(url: string, body, idDoc,);
	updatePublishing(url: string, body, idDoc,);
	create(url: string, body);
	getAll(url: string);
	updateAll(url: string, body);
	sendMail(url: string, id: string, emailOptions: SendMailParams);
	visualPdf(url: string, id: string);
}


