import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
declare var toastr: any;

@Component({
	selector: 'app-areplanifier-form',
	templateUrl: './areplanifier-form.component.html',
	styleUrls: ['./areplanifier-form.component.scss']
})
export class AreplanifierFormComponent implements OnInit {
	form
	processing = true;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,

		private translate: TranslateService,
		public dialogRef: MatDialogRef<AreplanifierFormComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { docType: string, doc },
		private fb: FormBuilder,
	) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
	}

	ngOnInit() {

		this.form = this.fb.group({
			description: ['', Validators.required],
		});
	}
	getTitle() {
		return 'Replanifier l\'intervention'
	}
	close() {
		this.dialogRef.close();

	}
	save() {
		this.update()
		this.dialogRef.close();
	}

	get f() { return this.form.controls; }

	update(): Promise<any> {
		return new Promise((resolve, reject) => {
			const url = this.data.docType + '/' + this.data.doc.id + '/Update/Status/UnDone'
			this.service.updateAll(url, this.form.value).subscribe(async res => {
				const text = this.translate.instant('toast.update-sucsess')
				toastr.success(text, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				//	this.routour(res.value);
				this.processing = false;
				resolve();
			}, async err => {
				console.log(err);
				const text = this.translate.instant('errors.serveur')
				toastr.warning(text, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				//this.processing = false;
				resolve();
			});
		})
	}
}
