import { Component, OnInit, Output, EventEmitter, Input, Inject } from '@angular/core';
import { Expense } from 'app/Models/Entities/Documents/Expense';
import { Modereglement } from 'app/Models/Entities/Parametres/Modereglement';
import { ParametrageCompte } from 'app/Models/Entities/Parametres/ParametrageCompte';
import { StatutDepense } from 'app/Enums/Statut/StatutDepense.Enum';
import { TypePaiement } from 'app/Enums/TypePaiement.Enum';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { StaticModeReglement } from 'app/Enums/StaticModeReglement.Enum';
import { ICalcule } from 'app/calcule/ICalcule';
import { Calcule } from 'app/calcule/Calcule';
import { FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { Credit } from 'app/Models/Entities/Documents/Credit';
import { TypeCompte } from 'app/Enums/Parameters/TypeCompte.enum';
import { StatutComptabilise } from 'app/Enums/StatutComptabilise.enum';
import { ACTION_API, ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { Typepaiement } from 'app/components/paiement-facture/paiement.component';
import { PaymentAdd } from 'app/Models/Model/PaymentAdd';

declare const toastr: any;
declare const jQuery: any;
declare const swal: any;

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'paiement-depense',
	templateUrl: './paiement.component.html',
	styleUrls: ['./paiement.component.scss']
})
export class PaiementDepenseComponent implements OnInit {
	@Output() refresh = new EventEmitter();
	@Input() depense: Expense;
	form;
	dateLang; // translate datepicker
	modesRegelement: Modereglement[] = [];
	comptes: ParametrageCompte[] = [];
	loading = false;
	indexModified;

	statutDepense: typeof StatutDepense = StatutDepense;
	statutComptabilise: typeof StatutComptabilise = StatutComptabilise;
	typePaiement: typeof TypePaiement = TypePaiement;
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	staticModeReglement: typeof StaticModeReglement = StaticModeReglement;
	calcule: ICalcule = new Calcule();
	reference;
	CheckValidPrice: any;
	MontantRest;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private fb?: FormBuilder,
		private translate?: TranslateService,
	) {
		this.form = this.fb.group({
			//  'montant': [null, [Validators.required], this.CheckValidPrice.bind(this)],
			'montant': [null, [Validators.required]],
			'idCaisse': [null, [Validators.required]],
			'datePaiement': [null, [Validators.required]],
			'idModePaiement': [null, [Validators.required]],
			'description': [null],
			'createAvoir': [false]
		})
	}

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.translate.get('datePicker').subscribe(text => {
			this.dateLang = text;
		});
		this.GetCompte('')
		this.GetModeRegelement('')

	}

	GetModeRegelement(search) {
		if (this.modesRegelement.length === 0) {
			this.service.getAll(ApiUrl.configModeRegelemnt)
				.subscribe(
					res => {
						this.modesRegelement = res.value
					}
				)
		}
	}

	GetCompte(search) {
		if (this.comptes.length === 0) {
			this.service.getAll(ApiUrl.configCompte)
				.subscribe(res => {
					this.comptes = res.value;
				})
		}
	}

	chargeLists() {
		this.form.reset()
		this.indexModified = null;
		this.form.controls['description'].setValue('Paiement dépense N° ' + this.depense.reference)
		this.form.controls['montant'].setValue(AppSettings.formaterNumber(this.getResterPayer()))
		this.form.controls['datePaiement'].setValue(new Date())
	}
	// Get information de form
	get f() { return this.form.controls; }


	// Calculate reste à payer

	getResterPayer() {
		if (this.depense && this.depense['orderDetails']['totalTTC'] !== undefined && this.depense.payments !== undefined) {
			this.MontantRest = this.depense['orderDetails']['totalTTC'] ?
				this.depense['orderDetails']['totalTTC'] - this.depense.payments.reduce((x, y) => x + y.amount, 0) : 0;
			return this.MontantRest;
		} else {
			return 0;
		}
	}
	CompareMontantInDb(montant: number, montantInDB: number) {

		const reste = montant - montantInDB;
		return (reste > 0 && reste < 0.01) ? montantInDB : montant
	}

	async savePaiement(indexModified) {
		if (this.form.valid) {

			this.loading = true;
			const values = this.form.value;
			const paiement: PaymentAdd = new PaymentAdd();
			paiement.description = values.description;
			paiement.operation = TypePaiement.Payment;
			paiement.amount = values.montant;
			paiement.datePayment = values.datePaiement;
			paiement.paymentMethodId = values['idModePaiement'];
			paiement.accountId = values.idCaisse;
			paiement.creditNoteId = null;
			paiement.invoices = [];
			paiement.expenses = [{
				'documentId': this.depense.id,
				'amount': paiement.amount
			}]
			// paiement.expensesId = this.depense.id.toString();
			if (indexModified) {
				this.service.update(ApiUrl.Payment, paiement, indexModified)
					.subscribe(arg => {
						if (arg) {
							this.successUpdate();
						} else {
							this.errorMontantIncorrect()

						}
					});

			} else {
				this.service.create(ApiUrl.Payment + ACTION_API.create, paiement).subscribe(res => {
					this.loading = false;
					if (res) {
						this.successfulAdd(res.value)
					} else {
						this.errorMontantIncorrect()
					}
				}, err => {
					this.loading = false;
					this.errorServer()
				})
			}
		} else {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAllPaiement, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		}
	}

	createPaiementAvoir(avoir: Credit) {
		this.loading = true;
		const paiement: PaymentAdd = new PaymentAdd();
		paiement.amount = avoir['totalTTC'] * (-1);
		paiement.datePayment = new Date(AppSettings.formaterDatetime(new Date().toString()));
		paiement.description = 'Paiement depense N° ' + this.depense.reference;
		paiement.type = Typepaiement.Expense;
		paiement.creditNoteId = avoir.id;
		paiement.expenses = [{
			amount: avoir['totalTTC'] * (-1),
			documentId: this.depense.id
		}];

		paiement.operation = TypePaiement.CreditNotePayment;
		paiement.paymentMethodId = +this.modesRegelement.filter(x => x.isDefault === true)[0].id;


		this.service.create(ApiUrl.Payment + ACTION_API.create, paiement).subscribe(value => {

			this.loading = false;
			if (value) {
				this.refresh.emit('')
				this.successfulAdd(value.value)
			} else {
				this.errorMontantIncorrect()
			}
		}, err => {
			this.loading = false,
				this.errorServer()
		}
		)
	}
	onChangeMoyenPaiement(value) {

		if (value === StaticModeReglement.Avoir) {
			this.form.controls['idCaisse'].setValue(null)
			this.form.controls['idCaisse'].setValidators([])
			this.form.controls['idCaisse'].updateValueAndValidity()
		} else if (value === StaticModeReglement.Espece) {
			const caisse = this.comptes.filter(x => x.type === TypeCompte.caisse)[0];
			if (caisse !== null) {
				this.form.controls['idCaisse'].setValue(caisse.id.toString())
			}
		} else {
			this.form.controls['idCaisse'].setValidators([Validators.required])
			this.form.controls['idCaisse'].updateValueAndValidity()
		}
	}

	// supprimer paiement
	removePaiement(index) {
		// const paiement = this.facture.facturePaiements[index].paiement;
		const paiement = this.depense.payments[index];
		this.translate.get('paiement.delete').subscribe(text => {
			swal({
				title: text.title,
				text: (paiement.type === TypePaiement.GroupedPayment ?
					text.questionGroupe : (paiement.type === TypePaiement.CreditNotePayment ? text.questionAvoirDeconste : text.question)),
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: true
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: true
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {

					this.loading = true;
					this.service.delete(ApiUrl.Payment, paiement.id.toString()).subscribe(res => {
						if (res) {
							this.loading = false;
							swal(text.success, '', 'success');
							this.refresh.emit('')
						}
					}, err => {
						this.loading = false;
						swal(text.failed, '', 'warning');
					})
				} else {
					toastr.success(text.failed, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			});
		});
	}

	// Successful ajoute
	successfulAdd(paiement?) {
		this.refresh.emit('')
		this.translate.get('paiement').subscribe(text => {
			jQuery('#ajouterPaiement').modal('hide');
			toastr.success(text.success, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			if (paiement !== null && paiement.operation === TypePaiement.CreditNotePayment) {
				toastr.success(text.avoirCreated + paiement.reference, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}
		})
	}

	successUpdate() {
		this.refresh.emit('')
		this.translate.get('paiement').subscribe(text => {
			jQuery('#ajouterPaiement').modal('hide');
			toastr.success(text.modifierSuccess, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		})
	}

	// Error server
	errorServer() {

		this.translate.get('errors').subscribe(text => {
			toastr.warning(text.errorServer, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		})
	}

	// Montant incorrect
	errorMontantIncorrect() {

		this.translate.get('errors').subscribe(text => {
			toastr.warning(text.invalid, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		})
	}

	// Charger les données dans la form de modification
	chargerModifierPaiement(index) {
		this.GetCompte('');
		this.GetModeRegelement('');
		this.indexModified = this.depense.payments[index].id;
		const paiement = this.depense.payments[index];
		this.form.controls['description'].setValue(paiement.description);
		this.form.controls['montant'].setValue(AppSettings.formaterNumber(paiement.amount));
		this.form.controls['idCaisse'].setValue(paiement['account'].id.toString());
		this.form.controls['idModePaiement'].setValue(paiement['paymentMethod'].id.toString());
		const datePaiement = new Date(paiement.datePayment.toString())
		this.form.controls['datePaiement'].setValue(datePaiement);
		jQuery('#ajouterPaiement').modal('show');
	}

}
