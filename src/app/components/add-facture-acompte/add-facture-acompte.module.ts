import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { MatNativeDateModule, MatTabsModule, MatSliderModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { AddFactureAcompteComponent } from './add-facture-acompte.component';
import { CommonModules } from 'app/common/common.module';
import { SliderModule } from '../../common/slider/slider.module';



export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/chantier/', '.json');
}

@NgModule({
	imports: [
		CommonModule,
		CommonModules,
		NgSelectModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient]
			},
			isolate: true
		}),
		FormsModule,
		MatNativeDateModule,
		MatDatepickerModule,
		MatCheckboxModule,
		MatAutocompleteModule,
		MatSelectModule,
		ReactiveFormsModule,
		DataTablesModule,
		NgbTooltipModule,
		MatTabsModule,
		SliderModule,
		CommonModule,
		MatSliderModule
	],
	exports: [],
	declarations: [

	],
	entryComponents: [AddFactureAcompteComponent]
})
export class AddFactureAcompteFormModule { }
