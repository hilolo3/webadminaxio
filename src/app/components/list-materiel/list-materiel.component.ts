import { Component, OnInit, Inject } from '@angular/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { MaterielComponent } from '../add-material/materiel.component';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { ActivatedRoute } from '@angular/router';
declare var swal: any;
declare var toastr: any;

@Component({
	selector: 'app-list-materiel',
	templateUrl: './list-materiel.component.html',
	styleUrls: ['./list-materiel.component.scss']
})
export class ListmaterielComponent implements OnInit {

	selected;
	materiel;
	id;
	items = [];
	search = '';
	client;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private route: ActivatedRoute,
		private dialog: MatDialog
	) {
		this.id = this.route.snapshot.params.id;
	}

	getDocument(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.getById(ApiUrl.Client, this.id).subscribe(res => {
				this.client = res.value;
				this.items = res.value.materials;
			});
		});
	}

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.getDocument();
	}

	AddMaterial(show, action?) {
		if (action === 'add') { this.selected = undefined }
		const dialogLotConfig = new MatDialogConfig();
		dialogLotConfig.width = '650px';
		dialogLotConfig.height = 'auto';
		dialogLotConfig.data = { selected: this.selected, show: show };
		const dialogRef = this.dialog.open(MaterielComponent, dialogLotConfig);
		dialogRef.afterClosed().subscribe((data) => {
			if (data !== undefined) {
				if (this.selected) {
					const index = this.items.findIndex(x => x.id === this.selected.id);
					this.items[index] = data;
				} else { this.items.push(data); }
				this.client.materials = this.items;
				this.update();
			}
		});
	}

	update() {
		this.service.update(ApiUrl.Client, this.client, this.id).toPromise();
	}




	delete() {
		this.translate.get('deleteMateriel').subscribe(text => {
			swal({
				title: text.title,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: true
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: true
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					const index = this.items.findIndex(x => x.id === this.selected.id);
					this.items.splice(index, 1);
					this.client.materials = this.items;
					this.update();
					toastr.success(text.success, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

				} else {
					swal(text.cancel, '', 'error');
				}
			});
		});
	}
	onMenuOpened(element: any): void {
		this.selected = element;
	}

}
