export enum OrderProductsDetailsType {
	/// <summary>
	/// the list of the product are selected and are included as a list
	/// </summary>
	List,
	/// <summary>
	/// the list of the product are selected as a file
	/// </summary>
	File
}