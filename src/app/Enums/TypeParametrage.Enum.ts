export enum TypeParametrage {
    numerotaion = 1,
    prix = 2,
    categorie = 3,
    typedocument = 5,
    tva = 4,
    parametrageDevis = 6,
    configMessagerie = 7,
    horaireTravail = 8,
    syncAgendaGoogle = 9,
    planComptable = 10,
    tvaPlanComptable = 11,
}

export enum TypeComptabilite {
    client = 1,
    fournisseur = 2,
    avoir = 3,
    caisse = 4,
    banque = 5

}