export enum QuoteStatus {
	in_progress = 'in_progress',
	accepted = 'accepted',
	refused = 'refused',
	canceled = 'canceled',
	billed = 'billed',
	signed = 'signed',
	late = 'late'
}