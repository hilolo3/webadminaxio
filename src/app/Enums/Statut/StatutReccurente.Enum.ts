export enum StatutReccurente {
  Brouillon = 'draft',
  Encours = 'in_progress',
  Termine = 'finished'
}