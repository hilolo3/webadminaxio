export enum DocumentRubricType {
  /// <summary>
  /// a Rubric of type Quote
  /// </summary>
  Quote = 1,

  /// <summary>
  /// a Rubric of type Orders
  /// </summary>
  Orders = 2,

  /// <summary>
  /// a Rubric of type Billing
  /// </summary>
  Billing = 3,

  /// <summary>
  /// a Rubric of type Custom
  /// </summary>
  Custom = 4,
}