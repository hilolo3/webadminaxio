import { Component, Input, AfterViewInit, OnChanges, OnDestroy } from '@angular/core';
import { FormGroup, FormControlName } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { textareaAnimations } from 'app/shared/animations/textarea.animations';

@Component({
	selector: 'app-input-textarea',
	templateUrl: './input-textarea.component.html',
	styleUrls: ['./input-textarea.component.scss'],
	animations: textareaAnimations
})
export class InputTextareaComponent implements AfterViewInit, OnChanges, OnDestroy {

	focused = false;
	selected = false;
	switchStatus = 'down';
	toolbarStatus = '';
	toolbarEnabled = false;
	subscription: Subscription;
	content = '';

	editorConfig: AngularEditorConfig = {
		editable: true,
		spellcheck: false,
		height: '10rem',
		enableToolbar: true,
		showToolbar: true,
		// toolbarHiddenButtons: [
		// 	[
		// 	],
		// 	[
		// 		'customClasses',
		// 		'link',
		// 		'unlink',
		// 		'insertImage',
		// 		'insertVideo',
		// 		'insertHorizontalRule',
		// 		'removeFormat',
		// 		'toggleEditorMode',
		// 		'undo',
		// 		'redo',
		// 		'strikeThrough',
		// 		'subscript',
		// 		'superscript',
		// 		'heading',
		// 		'fontName',
		// 		'justifyLeft',
		// 		'justifyCenter',
		// 		'justifyRight',
		// 		'justifyFull',
		// 	]
		// ]
	};

	/**
	 * The form in which the the input belongs to
	 */
	@Input() controlParent: FormGroup;

	/**
	 * The form control name
	 */
	@Input() controlName: FormControlName;

	/**
	 * The placeholder
	 */
	@Input() placeholder = '';

	/**
	 * The label
		*/
	@Input() label = '';

	/**
	 * The read status
	 */
	@Input() readOnly = false;

	/**
	 * Whether or not the textarea is expanded
	 */
	@Input() expanded = false;

	/**
	 * The value of the textarea
	 */
	@Input() value: string = '';

	/**
	 * The array of errors
	 */
	@Input() errors = '';

	/**
	 * The loading state of the component
	 */
	@Input() loading: boolean = false;

	/**
	 * Input ID
	 */
	@Input() id: string = '';

	/**
	 * Full height mode toggle
	 */
	@Input() fullHeight: boolean = false;

	constructor() {
		this.toolbarStatus = `${this.isSmallScreen() ? 'sm' : 'lg'}-hidden`;
	}

	ngOnChanges(): void {
		this.editorConfig.editable = !this.readOnly;
		this.updateTextarea(this.controlParent ? this.controlParent.controls[`${this.controlName}`].value : this.value);

	}

	ngOnDestroy(): void {
		if (this.controlParent && this.subscription) {
			this.subscription.unsubscribe();
		}
	}

	updateTextarea = (data: any): void => {
		this.value = data;
	}

	ngAfterViewInit(): void {

		setTimeout(() => {


			// Assigning the data
			const val = this.controlParent ? this.controlParent.controls[`${this.controlName}`].value : this.value;
			this.updateTextarea(val);

			if (this.controlParent) {
				this.subscription = this
					.controlParent
					.controls[`${this.controlName}`]
					.valueChanges
					.subscribe(data => {
						// Assigning the data
						this.updateTextarea(data);

						// Unsubscribing
						// subscription.unsubscribe();
					});
			}
		}, 1000);


	}

	/**
	 * Handles the change event of the input
	 *
	 * @param e: The event object
	 */
	onChange(e: any): void {
		// Updating the form-control's value
		this.controlParent.controls[`${this.controlName}`].setValue(e);
	}

	/**
	 * Handles the toggeling of the toolbar status
	 */
	onToolbarToggle(): void {

		// Checking if the textbox isn't on real only mode
		if (!this.readOnly) {

			// Toggeling the status
			this.toolbarEnabled = !this.toolbarEnabled;
			this.editorConfig.enableToolbar = this.toolbarEnabled;
			this.editorConfig.showToolbar = this.toolbarEnabled;

			// Updating the animation status
			this.toolbarStatus = `${this.isSmallScreen() ? 'sm' : 'lg'}-${this.toolbarEnabled ? 'visible' : 'hidden'}`;
			this.switchStatus = this.toolbarEnabled ? 'up' : 'down';
		}
	}

	/**
	 * Whether or not to display the required icon
	 */
	enableIcon(): boolean {
		return this.controlParent && this.controlParent.controls[`${this.controlName}`].errors
			&& this.controlParent.controls[`${this.controlName}`].errors.required
			&& this.label.length > 0;
	}


	/**
	 * Returns the control's form
	 */
	getForm() {
		return this.controlParent.get(`${this.controlName}`);
	}

	/**
	 * Checks if the screen is small
	 */
	isSmallScreen = (): boolean => window.innerWidth <= 480;
}
