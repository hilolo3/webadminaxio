import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTextareaComponent } from './input-textarea.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';

@NgModule({
	declarations: [InputTextareaComponent],
	imports: [
		CommonModule,
		ReactiveFormsModule,
		AngularEditorModule,
		FormsModule
	],
	exports: [InputTextareaComponent]
})
export class InputTextareaModule { }
