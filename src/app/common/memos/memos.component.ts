import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { Memo } from 'app/Models/Entities/Commun/Memo';
import { Constants } from 'app/shared/utils/constants';
import { PieceJoin } from 'app/Models/Entities/Commun/Commun';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { DomSanitizer } from '@angular/platform-browser';
import { DialogHelper } from 'app/libraries/dialog';
import { VisualiserPdfComponent } from '../visualiser-pdf/visualiser-pdf.component';
import { MatDialog } from '@angular/material';
import { HeaderService } from 'app/services/header/header.service';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { LocalElements } from 'app/shared/utils/local-elements';
declare var jQuery: any;
declare var toastr: any;

@Component({
	selector: 'common-memos',
	templateUrl: './memos.component.html',
	styleUrls: ['./memos.component.scss']
})
export class MemosComponent implements OnInit {

	// tslint:disable-next-line: no-input-rename
	@Input('memos') memos: Memo[];
	@Output() valueChange = new EventEmitter();
	@Output() download = new EventEmitter();
	@Input() showLoader = false;
	// tslint:disable-next-line: no-input-rename
	@Input('processIsStarting') processIsStarting = false;
	// tslint:disable-next-line: no-input-rename
	@Input('modificationPermission') modificationPermission = false;
	// tslint:disable-next-line: no-output-rename
	@Output('OnDeleteMemo') OnDeleteMemo = new EventEmitter();
	// tslint:disable-next-line: no-output-rename
	@Output('OnUpdateMemo') OnUpdateMemo = new EventEmitter();

	newMemos: Memo = new Memo();
	commentaire = '';
	files = null;
	formTitle = '';
	formType = '';
	memoEditIndex = null;
	logo;
	base64 = '';
	docType = '';

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private sanitizer: DomSanitizer,
		private dialog: MatDialog,
		private translate: TranslateService,
		private header: HeaderService
	) {
		this.translate.setDefaultLang('fr');
		this.translate.use(AppSettings.lang);
	}
	ngOnInit(): void {
		this.docType = localStorage.getItem(LocalElements.docType);

	}

	startUpload(event: FileList): void {
		const file = event.item(0)
		const reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => {
			const pieceJoin = new PieceJoin()
			// pieceJoin.name = AppSettings.guid()
			pieceJoin.fileType = Constants.getContentType(file.name.substring(file.name.lastIndexOf('.') + 1)).Mime;
			pieceJoin.fileName = file.name
			pieceJoin.content = reader.result.toString()
			this.newMemos.attachments.unshift(pieceJoin)
			this.files = null;
		}
	}

	onInit(): void {
		this.commentaire = '';
		this.newMemos = new Memo();
	}

	downloadFile(i, j) {
		this.download.emit(this.memos[i].attachments[j])
	}
	deleteFile(i) {
		this.newMemos.attachments.splice(i, 1)
	}

	save() {

		if (this.commentaire !== '' || this.newMemos.attachments.length > 0) {
			this.newMemos.comment = this.commentaire
			// if (this.formType === 'add') {
			this.newMemos.createdOn = new Date();
			this.valueChange.emit(this.newMemos);
			// } else {
			// 	this.OnUpdateMemo.emit({ memo: this.newMemos, index: this.memoEditIndex });
			// }
			this.emptyForm();
		}
	}

	editMemo(memo: any, index: number): void {
		this.OnUpdateMemo.emit({ memo, index });
	}
	deleteMemo(element) {
		const index = this.memos.findIndex(x => x.id === element.id)
		this.OnDeleteMemo.emit(index);
	}

	showDoc(i, j) {
		this.base64 = '';
		const base = this.memos[i].attachments[j];
		this.service.getById(ApiUrl.File, base['fileId']).subscribe(
			value => {
				const image = value.value;
				if (base.fileType === 'application/pdf') {
					// this.base64 = this.logo.replace(/^data:application\/(pdf);base64,/, '');
					// this.base64 = this.logo.replace(/^data:image\/*;charset=utf-8;base64,/, '');
					this.base64 = image.substring(image.lastIndexOf(';base64,') + 8);
					this.genererPDF();
				} else {
					this.logo = this.sanitizer.bypassSecurityTrustUrl(image);
					jQuery('#showImage').modal('show');
				}
			});
	}

	openModel(type, memo?: Memo, index?: number) {
		this.formType = type;
		if (type === 'add') {
			this.memoEditIndex = null;
			this.commentaire = '';
			this.newMemos.attachments = [];
			this.formTitle = 'Ajouter une note';
			// this.translate.get('commun').subscribe(text => this.formTitle = text.AjouterUneFicheTechnique);
		} else {
			this.memoEditIndex = index;
			this.formTitle = 'Modifier la note';
			// this.translate.get('commun').subscribe(text => this.formTitle = text.EditUneFicheTechnique);
			this.commentaire = memo.comment;
			const pieceJointes: PieceJoin[] = memo.attachments.map(pieceJointes => { return pieceJointes });
			this.newMemos.attachments = pieceJointes;
		}
		jQuery('#memoPopu').modal('show');
	}

	emptyForm(): void {
		this.commentaire = '';
		this.newMemos.attachments = [];
	}

	genererPDF() {
		DialogHelper.openDialog(
			this.dialog,
			VisualiserPdfComponent,
			DialogHelper.SIZE_MEDIUM,
			{ base64: this.base64, docType: '', doc: '' }
		).subscribe(async response => {
		});
	}

}
