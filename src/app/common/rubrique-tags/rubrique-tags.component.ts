import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { DocumentRubricType } from 'app/Enums/DocumentRubricType.Enum';
import { Rubrique } from 'app/Models/Entities/Documents/Rubrique';

@Component({
	selector: 'tags-rubrique',
	templateUrl: './rubrique-tags.component.html',
	styleUrls: ['./rubrique-tags.component.scss']
})
export class RubriqueTagsComponent implements OnInit, OnChanges {


	// tslint:disable-next-line: no-input-rename
	@Input('allTagsList') allTagsList: Array<string> = [];
	@Input('readOnly') readOnly: boolean = true;
	@Output('onTagsChange') onTagsChange = new EventEmitter();
	@Input('newTags') newTags: string[] = [];
	@ViewChild('inputString') public inputString;
	AutoComplete = [];
	showTagsValidation: boolean = false;
	@Input('load') load: { emptyList };
	@Input('size') size = 'large';
	constructor() { }


	ngOnInit() {
		if (this.allTagsList != undefined) {
			this.newTags = this.allTagsList;

		}
		setInterval(() => {
			this.onTagsChange.emit(this.newTags);
		}, 1000)
	}


	ngOnChanges(changes: SimpleChanges): void {
		this.load.emptyList = this.emptyList.bind(this);
	}
	emptyList() {
		this.newTags = [];
	}

	addNewTags(newTags, i) {
		const isE = ((this.newTags.filter(res => res.toLocaleLowerCase() === newTags.toLocaleLowerCase()).length) === 0);
		if (!isE) {
			this.showTagsValidation = true;
			setTimeout(() => {
				this.showTagsValidation = false;
			}, 2000);
			return;
		}
		this.newTags.push(newTags);

		this.AutoComplete = [];
		this.allTagsList.splice(i, 1);
		this.onTagsChange.emit(this.newTags);
		this.showTagsValidation = false;
		this.inputString.nativeElement.value = '';
	}
	addNewTagsFromInputClick() {
		const newTags = this.inputString.nativeElement.value;
		if (this.newTags.length > 0) {
			if (!this.checkTagIsUnique(this.newTags, newTags)) {
				this.showTagsValidation = true;
				setTimeout(() => {
					this.showTagsValidation = false;
				}, 2000);
				return;
			}
		}

		// tslint:disable-next-line: triple-equals
		if (newTags != '') {
			const tag = newTags
			this.newTags.push(tag);

			// this.allTagsList.forEach((tag, i) => {
			// 	if (tag.value === newTags) {
			// 		this.allTagsList.splice(i, 1);
			// 	}
			// });
			this.onTagsChange.emit(newTags);
			this.AutoComplete = [];
			this.inputString.nativeElement.value = '';
			this.showTagsValidation = false;
		}
	}
	checkTagIsUnique(tagsList, tag: string): boolean {
		let isE = true;
		if (tagsList.length > 0) {
			tagsList.forEach(t => {
				if (t.toLowerCase() === tag.toLowerCase()) {
					isE = false;
				}
			});
			isE = ((this.newTags.filter(res => res.toLocaleLowerCase() === tag.toLocaleLowerCase()).length) === 0);
		}
		return isE;
	}

	tagsInputkeyup(value, event) {
		if (event.code === 'Enter') {
			this.addNewTagsFromInputClick()
			return;
		}
		this.AutoComplete = [];
		if (this.allTagsList) {
			this.allTagsList.forEach((tag, i) => {
				if (tag.toUpperCase().indexOf(value.toUpperCase()) > -1 && value != '') {
					this.AutoComplete.push(tag);
				}
			});
		}

		this.onTagsChange.emit(this.newTags);
	}

	removeTag(tag, i) {
		if (tag.origine) {
			this.allTagsList.push(tag);
		}
		this.newTags.splice(i, 1);
		this.onTagsChange.emit(this.newTags);
		this.AutoComplete = [];
	}

	showAllTags() {

		if (this.AutoComplete.length === 0) {
			this.AutoComplete = this.allTagsList;
		} else if (this.AutoComplete = this.allTagsList) {
			this.AutoComplete = [];
		} else {
			this.AutoComplete = [];
		}

		// this.AutoComplete = (this.AutoComplete == this.allTagsList) ? [] : this.allTagsList;
	}

	conditionremoveTag(tag) {
		if (tag === 'Facturation' || tag === 'Devis' || tag === 'Commandes') {
			return false;
		} else {
			return true;
		}
	}

}
