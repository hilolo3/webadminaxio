import { Component, OnInit, Inject, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { ArticleType } from 'app/Models/Entities/Commun/article-type';
import { TranslateService } from '@ngx-translate/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { SortDirection } from 'app/Models/Model/filter-option';
import { ClassificationPrentId } from '../../../Enums/ClassificationPrentId';
import { fromEvent, pipe } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';

declare var toastr: any;

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'select-produit',
	templateUrl: './select-produit.component.html',
	styleUrls: ['./select-produit.component.scss']
})

export class SelectProduitComponent implements OnInit, AfterViewInit {
	search = '';
	page = 1;
	produits: any[] = [];
	selected: { product: any, quantity: number, type: number, remise: number }[] = []; // les Produit choisi dans popup
	loadingFinished = false;
	totalPage = 1;
	articleType: ArticleType = new ArticleType();
	Categorie = null;
	@ViewChild('searchInput') searchRef: ElementRef;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		public dialogRef: MatDialogRef<SelectProduitComponent>,
		@Inject(MAT_DIALOG_DATA)
		public data: {
			isAvoir: boolean, isIntervention: boolean
		}) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.getProduits();
	}

	ngAfterViewInit() {
		fromEvent(this.searchRef.nativeElement, 'keyup')
		.pipe(
			map((event: CustomEvent) => event.target['value'] ? event.target['value'].toLowerCase().trim() : ''),
			debounceTime(400),
			distinctUntilChanged()
		).subscribe(res => {
			this.searchProduit();
		})
	}


	getProduits() {
		/* if (this.data.isIntervention === true) {
			this.Categorie = ClassificationPrentId.Service;
		} */
		this.selected = [];
		this.loadingFinished = false;
		this.service.getAllPagination(ApiUrl.Produit, {
			// ClassificationPrentId: this.Categorie,
			SearchQuery: this.search,
			Page: this.page,
			PageSize: 50,
			OrderBy: 'reference',
			SortDirection: SortDirection.Descending
		}).subscribe(res => {
			if (res.value.length > 0) {
				this.produits = [...this.produits, ...res.value];
			}
			this.produits = this.produits.filter((l) => !this.selected.some(e => e.product.id === l.id));
			this.totalPage = res.pagesCount;
			this.loadingFinished = true;
		}, err => {
			console.log(err)
		});
	}


	searchProduit() {

		this.page = 1;
		this.produits = [];
		this.getProduits();

	}


	onScroll() {
		if (this.totalPage !== this.page) {
			this.page++;
			this.getProduits();
		}
	}

	checkElement(index, value) {
		this.selected.unshift({
			product: !this.data.isAvoir ? this.produits[index] : this.formatProduitIsAvoir(this.produits[index]),
			quantity: value != null ? +value : 1,
			type: this.articleType.produit,
			remise: 0
		});
		this.produits.splice(index, 1);
	}

	IncheckElement(index) {
		const produitSelected = !this.data.isAvoir ? this.selected[index].product :
			this.formatProduitIsAvoir(this.selected[index].product);
		this.produits.unshift(produitSelected as any);
		this.selected.splice(index, 1);
	}



	formatProduitIsAvoir(produits) {
		if (this.data.isAvoir) {
			produits.totalHT = produits.totalHT * (-1);
			produits.materialCost = produits.materialCost * (-1)
			produits.hourlyCost = produits.hourlyCost * (-1)
		}
		return produits;
	}

	changeQuantite(index, value, changed) {
		const produit = this.selected[index];
		if (value != null) { produit.quantity += value; }
		// tslint:disable-next-line:radix
		if (changed != null && changed !== '') { produit.quantity = parseInt(changed); }
		if (produit.quantity <= 0) { produit.quantity = 1; }
	}

	save() {
		if (this.selected.length === 0) {
			toastr.warning('text.serveur', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		} else {
			this.selected.map(x => x.product['categorieId'] = x.product['category'].id);
			this.dialogRef.close(this.selected);
		}
	}
}
