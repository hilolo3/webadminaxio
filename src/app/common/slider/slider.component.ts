import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { MatSliderChange } from '@angular/material';

@Component({
	selector: 'app-slider',
	templateUrl: './slider.component.html',
	styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

	//#region Properties

	@ViewChild('label') labelElement: ElementRef;

	@Input() value = 0;
	@Input() readOnly = false;

	@Output() change = new EventEmitter<any>();

	//#endregion

	//#region Lifcycle

	constructor() { }

	ngOnInit(): void { }

	//#endregion

	//#region Events

	onChange(e: MatSliderChange): void {
		this.value = e.value;
		this.change.emit(parseInt(e.value as any, 10));
	}

	// #endregion

	//#region Methods

	formatDisplay = () => `${(this.value || 0).toFixed(2)} %`;

	getLabelPos(): number | string {
		let res: string | number = 0;

		if (this.labelElement && this.labelElement.nativeElement) {
			const width = this.labelElement.nativeElement.offsetWidth;
			const ratio = width * (this.value / 100);

			res = ratio <= 25.39
				? `calc(${this.value}% + 25.39px)`
				: ratio >= width - 25.39
					? `calc(${this.value}% - 25.39px)`
					: `${this.value}%`;
		}

		return res;
	}

	//#endregion
}
