import { Component, Input, Output, OnInit, Inject, EventEmitter, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { HelperFunctions } from 'app/libraries/helper';



@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {

  dataSub: Subscription;

  @Input() data: any;
  @Input() dataEmitter = new EventEmitter<any[]>();
  @Output() subCategoryDelete: EventEmitter<number>;
  @Output() subCategoryUpdate: EventEmitter<number>;


  constructor() {
    this.subCategoryDelete = new EventEmitter();
    this.subCategoryUpdate = new EventEmitter();
  }


  ngOnInit(): void {
    this.dataSub = this.dataEmitter.subscribe((data) => {
      this.data = data
    });
  }

  ngOnDestroy(): void {
    if (this.dataSub) {
      this.dataSub.unsubscribe();
    }
  }


  onDelete(e: MouseEvent,index: number) {
    e.stopPropagation();
    this.subCategoryDelete.emit(index);
  }

  onUpdate(e: MouseEvent,index: number) {
    e.stopPropagation();
    this.subCategoryUpdate.emit(index);
  }

}
