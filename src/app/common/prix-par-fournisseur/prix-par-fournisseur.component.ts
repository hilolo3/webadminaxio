import { Component, OnInit, Input, OnChanges, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';
import { FormBuilder, Validators } from '@angular/forms';
import { AppSettings } from 'app/app-settings/app-settings';
import { Supplier } from 'app/Models/Entities/Contacts/Supplier';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { PrixParFournisseur_TypeOfForm, PrixParFournisseur_validation } from 'app/Models/Model/prixParFournisseur';

declare var swal: any;
declare var jQuery: any;
declare var toastr: any;

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'prix-par-fournisseur',
	templateUrl: './prix-par-fournisseur.component.html',
	styleUrls: ['./prix-par-fournisseur.component.scss']
})
export class PrixParFournisseurComponent implements OnInit, OnChanges {

	// tslint:disable-next-line:no-input-rename
	@Input('PrixParFournisseur') public ListPrixParFournisseur = [];
	// tslint:disable-next-line:no-input-rename
	@Input('idProduit') idProduit = -1;
	// tslint:disable-next-line:no-input-rename
	@Input('readOnly') readOnly = false;
	// tslint:disable-next-line:no-input-rename
	@Input('size') size = 'small';
	// tslint:disable-next-line:no-input-rename
	@Input('getList') getList: { return };
	// tslint:disable-next-line:no-input-rename
	@Input('validation') validation: PrixParFournisseur_validation = new PrixParFournisseur_validation();

	public listFournisseurs = [];
	listFournisseursNotModified: Supplier[] = [];
	form: any = null;
	typeOfForm: PrixParFournisseur_TypeOfForm = null;
	TypeOfFormEnum: typeof PrixParFournisseur_TypeOfForm = PrixParFournisseur_TypeOfForm;
	ListformLabels: { type: PrixParFournisseur_TypeOfForm, labels: { title: string, btnSave: string, btnClose: string } }[] = [];
	formLabels: { title: string, btnSave: string, btnClose: string } = null;
	updateUserIndex: number = null;

	swalWarningConfig = (text: any) => {
		return {
			title: text.title,
			text: text.question,
			icon: 'warning',
			buttons: {
				cancel: {
					text: text.cancel,
					value: null,
					visible: true,
					className: '',
					closeModal: false
				},
				confirm: {
					text: text.confirm,
					value: true,
					visible: true,
					className: '',
					closeModal: false
				}
			}
		};
	};

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private formBuilder: FormBuilder
	) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.setFormLabels();
		this.form = this.formBuilder.group({
			prix: [null, [Validators.required]],
			idFournisseur: [null, [Validators.required]],
			default: [null]
		});
	}

	async  ngOnChanges() {
		this.getFournisseurList();
		if (this.getList !== undefined) {
			this.getList.return = await this.emit.bind(this);
		}
	}

	async emit(operation, callback): Promise<void> {
		switch (operation) {
			case 'getList':
				this.returnDataToSave(callback);
				break;
			case 'reload':
				await this.reload();
				break;
		}
	}

	async  reload(): Promise<void> {
		this.listFournisseurs = null;
		this.updateUserIndex = null;
		this.form.reset();
		await this.getFournisseurList();
	}


	setFormLabels() {
		this.translate.get('PrixParFournisseur.labels').subscribe(labels => {
			this.ListformLabels = [
				{
					type: this.TypeOfFormEnum.add,
					labels: {
						title: labels['model_add_title'],
						btnSave: labels['ajouterPrixParFournisseur_btn'],
						btnClose: labels['btn_Close']
					}
				},
				{
					type: this.TypeOfFormEnum.update,
					labels: {
						title: labels['model_update_title'],
						btnSave: labels['modifierPrixParFournisseur_btn'],
						btnClose: labels['btn_Close']
					}
				}
			]
		});
	}


	getFormLabels(): { title: string, btnSave: string, btnClose: string } {
		return this.ListformLabels.filter(x => x.type === this.typeOfForm)[0].labels;
	}

	spliceUpdated() {
		if (this.updateUserIndex != null) {
			const i = this.findIndex(this.ListPrixParFournisseur[this.updateUserIndex].supplierId);
			if (i !== -1) {
				this.removeFromlistFournisseurs(i);
				this.updateUserIndex = null
			}
		}
	}

	async openAddForm(): Promise<void> {
		await this.getFournisseurList();
		this.spliceUpdated();
		this.checkIfDefaultCheckBoxIsDisabled();
		this.form.reset();
		this.typeOfForm = PrixParFournisseur_TypeOfForm.add;
		this.formLabels = this.getFormLabels();
		jQuery('#PopUpForm').modal('show');
		jQuery('#PopUpForm').modal('hide');
		jQuery('#PopUpForm').modal('show');
	}

	async openEditForm(prixParFournisseur, index: number): Promise<void> {

		await this.getFournisseurList();
		this.spliceUpdated();

		if (this.listFournisseurs.filter(x => x.id === prixParFournisseur.fournisseur.id).length === 0) {
			this.pushInlistFournisseurs(prixParFournisseur.fournisseur);
		}
		this.updateUserIndex = index;
		this.checkIfDefaultCheckBoxIsDisabled();
		this.form.reset();
		this.typeOfForm = PrixParFournisseur_TypeOfForm.update;
		this.formLabels = this.getFormLabels();
		this.form.controls['prix'].setValue(prixParFournisseur['price']);
		this.form.controls['idFournisseur'].setValue(prixParFournisseur['supplierId']);
		this.form.controls['default'].setValue(prixParFournisseur['isDefault']);
		jQuery('#PopUpForm').modal('show');
	}

	saveData(): void {

		if (!this.checkFormValidation()) { return; }
		switch (this.typeOfForm) {
			case this.TypeOfFormEnum.add:
				this.add();
				break;
			case this.TypeOfFormEnum.update:
				this.edit();
				break;
		}
	}

	add(): void {
		if (this.form.value.default == null && this.ListPrixParFournisseur.length === 0) {
			this.form.value.default = true;
		}
		const prixParFournisseur = {
			supplierId: this.form.value.idFournisseur,
			price: this.form.value.prix,
			isDefault: this.form.value.default,
			fournisseur: this.listFournisseurs[this.findIndex(this.form.value.idFournisseur)]
		}
		if (prixParFournisseur.isDefault === true && this.ListPrixParFournisseur.length > 0) {
			this.ListPrixParFournisseur.map(x => x.isDefault = 0);
		}
		this.pushInPrixParFournisseur(prixParFournisseur);
		this.removeFromlistFournisseurs(this.findIndex(prixParFournisseur.supplierId));
		jQuery('#PopUpForm').modal('hide');
	}

	edit(): void {
		if (this.form.value.default) { this.ListPrixParFournisseur.map(x => x.isDefault = 0); }
		this.ListPrixParFournisseur[this.updateUserIndex].price = this.form.value.prix;
		this.ListPrixParFournisseur[this.updateUserIndex].supplierId = this.form.value.idFournisseur;
		this.ListPrixParFournisseur[this.updateUserIndex].isDefault = this.form.value.default;
		if (this.ListPrixParFournisseur[this.updateUserIndex].isDefault == null && this.ListPrixParFournisseur.length === 0) {
			this.ListPrixParFournisseur[this.updateUserIndex].isDefault = 1;
		}
		this.ListPrixParFournisseur[this.updateUserIndex].fournisseur = this.listFournisseurs[this.findIndex(this.form.value.idFournisseur)];
		this.removeFromlistFournisseurs(this.findIndex(this.form.value.idFournisseur));
		this.updateUserIndex = null;
		jQuery('#PopUpForm').modal('hide');
	}

	remove(index: number): void {
		this.translate.get('PrixParFournisseur.delete').subscribe(async text => {
			await this.getFournisseurList();
			swal(this.swalWarningConfig(text)).then(isConfirm => {
				if (isConfirm) {
					this.pushInlistFournisseurs(this.ListPrixParFournisseur[index].fournisseur);
					this.removeFromPrixParFournisseur(index);
					if (this.ListPrixParFournisseur.length === 1) {
						this.ListPrixParFournisseur.map(x => x.isDefault = 1);
					}

					this.updateUserIndex = null;
					swal(text.success, '', 'success');
				} else {
					swal(text.cancel, '', 'error');
				}
			});
		});
	}

	pushInlistFournisseurs(fournisseur): void {
		const id = fournisseur.id;
		if (this.listFournisseurs.filter(x => id === x.id).length === 0) {
			this.listFournisseurs.unshift(fournisseur);
		}
	}

	pushInPrixParFournisseur(prixParFournisseur): void {
		const id = prixParFournisseur.supplierId;
		if (this.ListPrixParFournisseur.filter(x => id === x.supplierId).length === 0) {
			this.ListPrixParFournisseur.unshift(prixParFournisseur);
		}
	}

	removeFromlistFournisseurs(index: number): void {
		const id = this.listFournisseurs[index].id;
		if (this.listFournisseurs.filter(x => id === x.id).length !== 0) {
			this.listFournisseurs.splice(index, 1);
		}
	}

	removeFromPrixParFournisseur(index: number): void {
		const id = this.ListPrixParFournisseur[index].supplierId;
		if (this.ListPrixParFournisseur.filter(x => id === x.supplierId).length !== 0) {
			this.ListPrixParFournisseur.splice(index, 1);
		}
	}

	returnDataToSave(callback): void {
		let response = {
			list: [],
			validation: {
				defaultPrice: false,
				nbOfPrices: false,
				isValid: false
			}
		}

		const defaultPrice = this.validation.defaultPriceRequired ?
			(this.ListPrixParFournisseur.filter(x => x.isDefault === true || x.isDefault === 1).length === this.validation.nbDefaultPrice) : true;
		if (!defaultPrice) {
			// this.dispalyMsgError();
			callback(response);
			return;
		}

		const nbOfPrices = this.ListPrixParFournisseur.length >= this.validation.NbOfPricesRequired;
		if (!nbOfPrices) {
			// this.dispalyMsgError();
			callback(response);
			return;
		}

		this.ListPrixParFournisseur = this.ListPrixParFournisseur.map(element => {
			element.supplierId = element.supplierId;
			element.isDefault = element.isDefault ? 1 : 0;
			element.fournisseur = null;
			return element;
		});

		response = {
			list: this.ListPrixParFournisseur,
			validation: {
				defaultPrice,
				nbOfPrices,
				isValid: defaultPrice && nbOfPrices
			}
		}
		callback(response);
	}

	checkIfDefaultCheckBoxIsDisabled(): void {
		if (this.updateUserIndex != null && +this.ListPrixParFournisseur[this.updateUserIndex].isDefault === 1) {
			this.form.get('default').enable();
		} else {
			const length = this.ListPrixParFournisseur.filter(x => x.isDefault === true).length;
			if (length >= this.validation.nbDefaultPrice) {
				this.form.get('default').enable();
			} else {
				this.form.get('default').enable();
			};
		}
	}

	checkFormValidation(): boolean {
		this.form.controls['idFournisseur'].setValue(this.form.value.idFournisseur === null ? null : this.form.value.idFournisseur);
		if (this.form.invalid) {
			this.form.touched = true;
			return false;
		} else {
			return true;
		}
	}


	dispalyMsgError(): void {
		this.translate.get('PrixParFournisseur.errors').subscribe(text => {
			toastr.warning(text.required, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		})
	}

	closeForm() {
		jQuery('#PopUpForm').modal('hide');
	}

	findIndex(id: string): number {
		const ids = this.listFournisseurs.map(x => {
			if (x !== undefined) {
				return x.id
			}
		});
		return ids.indexOf(id);
	}

	getFournisseurList() {
		return new Promise((resolve, reject) => {
			if (this.listFournisseurs != null && this.listFournisseurs.length !== 0) {
				resolve()
			} else {
				this.service.getAll(ApiUrl.Fournisseur).subscribe(res => {
					this.listFournisseurs = res.value;
					if (this.ListPrixParFournisseur !== undefined && this.ListPrixParFournisseur.length !== 0) {
						if (this.ListPrixParFournisseur[0].fournisseur === undefined || this.ListPrixParFournisseur[0].fournisseur === null) {
							this.ListPrixParFournisseur.map(element => {
								const ii = this.findIndex(element.supplierId);
								element.fournisseur = this.listFournisseurs[ii];
								this.removeFromlistFournisseurs(ii);
							});
						} else {
							this.ListPrixParFournisseur.map(element => {
								const ii = this.findIndex(element.supplierId);
								this.removeFromlistFournisseurs(ii);
							});
						}
					}
					resolve();
				});
			}
		});
	}

	get f() { return this.form.controls; }
}
