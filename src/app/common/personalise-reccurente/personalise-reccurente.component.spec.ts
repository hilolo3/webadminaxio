import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonaliseReccurenteComponent } from './personalise-reccurente.component';

describe('PersonaliseReccurenteComponent', () => {
  let component: PersonaliseReccurenteComponent;
  let fixture: ComponentFixture<PersonaliseReccurenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonaliseReccurenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonaliseReccurenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
