import { ICalcule } from './ICalcule';
import { CalculTva } from 'app/Models/Model/calcul-tva';
import { ResultatCalculModel } from 'app/Models/Model/ResultatCalculModel';
import { TypeValue } from 'app/Enums/typeValue.Enums';

export class Calcule implements ICalcule {

	margeBrut(articles, globalTotalHT: number, remiseGloabl: number, typeRemiseGloabl: TypeValue): number {
		// tslint:disable-next-line:prefer-const
		const sumPrixAchatArticles = articles.reduce((x, y) => x + ((y.productSuppliers == null ||
			y.productSuppliers.filter(x => x.isDefault === 1).length === 0) ? 0 :
			y.productSuppliers.filter(x => x.isDefault === 1)[0].price) * y.qte, 0);
		const sumPrixArticles = articles.reduce((x, y) => x + y.totalHT, 0)
		articles.map(x => x.remise == null ? x.remise = 0 : x.remise);
		const sumRemiseArticles = articles.reduce((x, y) => x + y.remise, 0);
		if (typeRemiseGloabl === TypeValue.Amount) {
			// tslint:disable-next-line:no-var-keyword
			var valeurRemiseGloabl = remiseGloabl;
		} else {
			// tslint:disable-next-line:no-var-keyword
			var valeurRemiseGloabl = (globalTotalHT * (remiseGloabl / 100));
		}
		return sumPrixArticles - sumPrixAchatArticles - sumRemiseArticles - valeurRemiseGloabl;
	}

	totalTTC(globalTotalHT: number, globalTotalHTRemise: number, calculTvas: CalculTva[], remiseGloabl: number): number {
		const sumTVA = calculTvas.reduce((x, y) => x + y.totalTVA, 0);
		if (remiseGloabl && remiseGloabl !== 0) {
			return (globalTotalHTRemise + sumTVA);
		} else {
			return (globalTotalHT + sumTVA)
		}
	}

	totalHtRemise(globalTotalHT: number, remiseGloabl: number, typeRemiseGloabl: TypeValue): number {
		if (typeRemiseGloabl === TypeValue.Amount) {
			return (globalTotalHT - remiseGloabl);
		} else {
			return (globalTotalHT - (globalTotalHT * (remiseGloabl / 100)));
		}
	}

	calculVentilationRemiseDepense(articles: any, globalTotalHT: number, remiseGloabl: number, typeRemiseGloabl: TypeValue, id): CalculTva[] {
		const calculTvas: CalculTva[] = [];
		let groubTva = [];
		if (articles.length > 0) {
			articles = articles.map(article => {
				const tva: string = article.vat.toString();
				article.vat = parseFloat(tva);
				return article;
			})

			const groupTvaDistinct = articles.filter(
				(value, index, self) => self.map(e => e.vat).indexOf(value.vat) === index
			);
			groubTva = groupTvaDistinct.map(x => x.vat).sort((a, b) => a - b);
			if (groubTva.length != 0) {

				groubTva.forEach(tva => {
					const calcule = new CalculTva();
					calcule.tva = tva;
					const produits = articles.filter(x => x.vat === tva);
					let totalHtProduit = 0;
					if (produits.length != 0) {

						produits.forEach(element => {
							if (id !== null) {
								const prixParFournisseur = element.productSuppliers.filter(x => x.supplierId == id)[0];
								const prix = prixParFournisseur.price;
								totalHtProduit = totalHtProduit + (prix * element.quantity);
							}
						});
					} else {
						totalHtProduit

					}
					if (typeRemiseGloabl === TypeValue.Amount) {
						let percente = (totalHtProduit / globalTotalHT);
						percente = isNaN(percente) ? 0 : percente;
						calcule.totalHT = totalHtProduit - percente * remiseGloabl;
						calcule.totalTVA = calcule.totalHT * (tva / 100);
						calcule.totalTTC = calcule.totalTVA + calcule.totalHT;
					} else {
						calcule.totalHT = totalHtProduit - ((totalHtProduit * remiseGloabl) / 100);
						calcule.totalTVA = calcule.totalHT * (tva / 100);
						calcule.totalTTC = calcule.totalTVA + calcule.totalHT;
					}
					calculTvas.push(calcule);
				});
			}
			else {
				let calculTvas: CalculTva[] = [];
				const calcule = new CalculTva();
				calcule.totalHT = 0;
				calcule.totalTVA = 0;
				calcule.totalTTC = 0;
			}

			return calculTvas;
		}
		else {
			let calculTvas: CalculTva[] = [];
			const calcule = new CalculTva();
			calcule.totalHT = 0;
			calcule.totalTVA = 0;
			calcule.totalTTC = 0;
			return calculTvas;
		}


	}

	calculVentilationRemise(articles: any, globalTotalHT: number, remiseGloabl: number, typeRemiseGloabl: TypeValue): CalculTva[] {

		const calculTvas: CalculTva[] = [];
		let groubTva = [];
		const groupTvaDistinct = articles.filter(
			(value, index, self) => self.map(e => e.vat).indexOf(value.vat) === index
		);
		groubTva = groupTvaDistinct.map(x => x.vat).sort((a, b) => a - b);
		groubTva.forEach(tva => {
			const calcule = new CalculTva();
			calcule.tva = +tva;
			const produits = articles.filter(x =>  +x.vat === +tva);
			const totalHtProduit = produits.reduce((x, y) => x + (y.totalHT * y['quantity']), 0);
			if (typeRemiseGloabl === TypeValue.Amount) {
				let percente = (totalHtProduit / globalTotalHT);
				percente = isNaN(percente) || percente === Infinity ? 0 : percente;
				calcule.totalHT = totalHtProduit - percente * remiseGloabl;
				calcule.totalTVA = calcule.totalHT * (+tva / 100);
				calcule.totalTTC = calcule.totalTVA + calcule.totalHT;
			} else {
				calcule.totalHT = totalHtProduit - ((totalHtProduit * remiseGloabl) / 100);
				calcule.totalTVA = calcule.totalHT * (+tva / 100);
				calcule.totalTTC = calcule.totalTVA + calcule.totalHT;
			}
			calculTvas.push(calcule);
		});
		return calculTvas;
	}

	totalHt(articles: any[]): number {
		return articles.reduce((x, y) => x + y.totalHT, 0);
	}

	totalTTCArticle(articleTotalHT: number, tva: number): number {
		return articleTotalHT * (tva / 100) + articleTotalHT
	}

	totalHTArticle(prix: number, quantite: number, remise: number): number {
		if (remise) {
			return (prix * quantite) - remise;
		} else {
			return (prix * quantite);
		}
	}

	calculGenerale(articles, remiseGloabl: number, typeRemiseGloabl: TypeValue): ResultatCalculModel {
		articles.forEach(article => {
			article.totalHT = this.totalHTArticle(article.prix, article.quantity, article.remise);
			article.totalTTC = this.totalTTCArticle(article.totalHT, article.vat);
		});
		const totalHT = this.totalHt(articles);
		const calculTvas = this.calculVentilationRemise(articles, totalHT, remiseGloabl, typeRemiseGloabl);
		const totalHtRemise = this.totalHtRemise(totalHT, remiseGloabl, typeRemiseGloabl);
		const totalTTC = this.totalTTC(totalHT, totalHtRemise, calculTvas, remiseGloabl);
		const margeBrut = this.margeBrut(articles, totalHT, remiseGloabl, typeRemiseGloabl);
		return {
			articles: articles,
			totalHT: totalHT,
			calculTvas: calculTvas,
			totalHtRemise: totalHtRemise,
			totalTTC: totalTTC,
			margeBrut: margeBrut
		};
	}

}